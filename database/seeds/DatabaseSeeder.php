<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		 $this->call('ConversionCompaniesSeeder');
		 $this->call('ConversionSeeder');
	}

}

class ConversionCompaniesSeeder extends Seeder {

	public function run()
	{
		DB::table('conversion_companies')->delete();

		$options = array('' => 'Conversion By....', 'braun' => 'Braun', 'vmi' => 'VMI', 'eldorado' => 'Eldorado', 'ams' => 'AMS', 'van-action' => 'Van Action', 'rollx' => 'Rollx', 'non-lowered-floor' => 'Non Lowered Floor', 'other' => 'Other');

		foreach($options as $option)
		{
		$conversion = new ConversionCompany;
		$conversion->name = $option;
		$conversion->save();
		}
	}
}

class ConversionSeeder extends Seeder {

	public function run()
	{
		DB::table('conversions')->delete();

		$options = array(
				array('company' => 3, 'conversion' => 'Entervan 10\" Drop'),
				array('company' => 3, 'conversion' => 'Entervan XT 14\" Drop'),
				array('company' => 3, 'conversion' => 'Rampvan'),
				array('company' => 3, 'conversion' => 'Vision Rear Entry'),
				array('company' => 4, 'conversion' => 'Summit'),
				array('company' => 4, 'conversion' => 'Northstar'),
				array('company' => 5, 'conversion' => 'Amerivan 10\" Drop'),
				array('company' => 5, 'conversion' => 'Amerivan 14\" Drop'),
				array('company' => 6, 'conversion' => 'AMS Conversion'),
				array('company' => 7, 'conversion' => 'Van Action Conversion'),
				array('company' => 8, 'conversion' => 'Rollx Conversion'),
				array('company' => 9, 'conversion' => 'Non Lowered Floor'),
				array('company' => 10, 'conversion' => 'Other'),

			);

		foreach($options as $option)
		{
		$conversion = new Conversion;
		$conversion->name = $option['conversion'];
		$conversion->company_id = $option['company'];
		$conversion->save();
		}
	}
}

