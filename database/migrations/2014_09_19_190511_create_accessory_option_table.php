<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccessoryOptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accessory_option', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('accessory_id')->unsigned()->index();
			$table->foreign('accessory_id')->references('id')->on('accessories')->onDelete('cascade');
			$table->integer('option_id')->unsigned()->index();
			$table->foreign('option_id')->references('id')->on('options')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accessory_option');
	}

}
