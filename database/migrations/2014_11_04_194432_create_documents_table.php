<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';
			$table->increments('id');
			$table->string('title');
			$table->text('body');
			$table->integer('category');
			$table->timestamps();

			 
		});
		DB::statement('ALTER TABLE documents ADD FULLTEXT search(title, body)');
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents', function($table) {
	   $table->dropIndex('search');
	  });
	  Schema::drop('documents');
	}

}
