@extends('layouts.login')

@section('content')
	<div class="container">
		
		<form action="{{ action('RemindersController@store') }}" method="POST" class="form-signin">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<img src="/assets/images/bd_logo_new.png" width="100%"  />
			<div>
				<h2 class="form-signin-heading text-center">Reset Password</h2>
		    	<input type="email" name="email" class="form-control" placeholder="Email Address">
		   </div>
		    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Send Reminder">
		     <a href="/"><i class="fa fa-arrow-left fa-fw"></i> Back to Login</a>
		</form>
	</div>
@stop