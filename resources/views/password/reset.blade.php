@extends('layouts.login')

@section('content')
<div class="container">
	<form action="{{ action('RemindersController@postReset') }}" method="POST" class="form-signin">
		<img src="/assets/images/bd_logo_new.png" width="100%"  />
		<h2 class="form-signin-heading text-center">Reset Password</h2>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	    <div>
	    	<input type="email" name="email" class="form-control" placeholder="Email">
	    </div>
	    <div>
	    	<input type="password" name="password" class="form-control" placeholder="Password">
	    </div>
	    <div>
	    	<input type="password" name="password_confirmation" class="form-control" placeholder="Password Confirm">
	    </div>
	    <input type="submit" value="Reset Password" class="btn btn-lg btn-primary btn-block" >
	    <a href="/"><i class="fa fa-arrow-left fa-fw"></i> Back to Login</a>
</form>
</div>
@stop