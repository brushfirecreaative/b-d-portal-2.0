<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <style>
          table {
            border-spacing: 0;
            border-collapse: collapse;
          }
          td,
          th {
            padding: 0;
          }
        </style>
    </head>
    <body>
      <table style="width: 550px; margin:0 auto; ">
        <tr>
          <td style="text-align: center;">
            <img src="http://bdidealers.com/assets/images/b%26d_logo_new.png" width="137.5" />
          </td>
        </tr>
        <tr>
            <td>
                <h2 style="text-align: center;">Congratulations</h2>

                <p style="text-align: center;">Your account has been approved.<br> You may now log in with your username bellow and the password you registered with:</p>
                <p style="text-align: center;">{{ $username }}</p>
              
                <p style="text-align: center;">
                <a href="{{ URL::to('login') }}" class="btn">Take me to B&amp;D Dealer Portal!</a>
                </p>
           </td>
        </tr>
      </table>
    </body>
</html>