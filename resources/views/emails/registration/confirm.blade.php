<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <style>
          table {
            border-spacing: 0;
            border-collapse: collapse;
          }
          td,
          th {
            padding: 5px 0;
          }
        </style>
    </head>
    <body>
      <table style="width: 550px; margin:0 auto;">
        <tr>
          <td style="text-align: center;">
            <img src="http://bdidealers.com/assets/images/b%26d_logo_new.png" width="137.5" />
          </td>
        </tr>
        <tr>
            <td>
                <h2 style="text-align: center;">Confirm New User</h2>

                <p style="text-align: center;">The following user has registered for an account for your Dealer. <br />Their status is pending your confirmation.</p>
                <table style="margin:0 auto">
                    <tr>
                        <td><b>Name:</b> </td>
                        <td>{{ $tech->prop_name }}</td>
                    </tr>
                    <tr>
                        <td><b>Email:</b> </td>
                        <td>{{ $tech->email }}</td>
                    </tr>
                     <tr>
                        <td><b>Position:</b> </td>
                        <td>{{ $tech->position }}</td>
                    </tr>
                    <tr>
                        <td><b>Phone:</b> </td>
                        <td>{{ $tech->phone }}</td>
                    </tr>
                  </table>
    
                <p style="text-align: center;">
                <a href="{{ URL::to('register/confirm/' . $confirmation_code) }}" class="btn">Confirm User</a>
                </p>
           </td>
        </tr>
      </table>
    </body>
</html>