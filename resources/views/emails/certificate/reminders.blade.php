<!DOCTYPE html>
<html lang="en-US">
    <head>
      <title>Your B&D Certificate is About to Expire</title>
        <meta charset="utf-8">
        <style>
           .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
               -moz-user-select: none;
                -ms-user-select: none;
                    user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #fff;
            background-color: #428bca;
            border-color: #357ebd;

          }
          table {
            border-spacing: 0;
            border-collapse: collapse;
          }
          td,
          th {
            padding: 0;
          }
        </style>
    </head>
    <body>
      <table style="width: 550px; margin:0 auto; ">
        <tr>
          <td style="text-align: center;">
            <img src="http://bdidealers.com/assets/images/b%26d_logo_new.png" width="25%" />
          </td>
        </tr>
        <tr>
            <td>
                <h2 style="text-align: center; color:red;">Reminder!</h2>

                <p style="text-align: center;">Your <b><i>{{ $certificate->name }}</i></b> will expire on <b> {{ $date_expires }}</b>. <br>Please make sure to retake the certifcation before this date. </p>
              
                <p style="text-align: center;">
                <a href="{{ URL::to('certificate') }}" class="btn">Retake Test Now</a>
                </p>
           </td>
        </tr>
      </table>
    </body>
</html>