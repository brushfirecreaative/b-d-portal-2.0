<html>
<head>
</head>
<body>
<h3>Congratulations on completing your certificate!</h3>
<p>Attached is your Certificate. <br> You may also print your certificate from the B&amp;D Dealer Protal at any time.</p>
<b>Technician:</b> {{ $user->prop_name }}<br>
<b>Dealer:</b> {{ $dealer->name}}<br>
<b>Corporation:</b> {{ $dealer->parent->name }}<br>
<b>Certificate:</b> {{ $cert->name }}
</body>
</html>