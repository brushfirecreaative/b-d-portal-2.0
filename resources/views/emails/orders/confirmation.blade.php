<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <title>New Order</title>
        <style>
           .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
               -moz-user-select: none;
                -ms-user-select: none;
                    user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #fff;
            background-color: #428bca;
            border-color: #357ebd;

          }
          table {
            border-spacing: 0;
            border-collapse: collapse;
          }
          td,
          th {
            padding: 5px;
            vertical-align: top;
          }
          h2{
            border-bottom: 1px #25699C solid;
          }
          h3{
            padding: 5px;
            margin: 0;
          }        
      </style>
    </head>
    <body style="width: 600px">
      <table style="width: 600px; border: solid 10px #25699C">
        <tr>
          <td style="width: 50%">
            <img src="http://bdidealers.com/assets/images/b%26d_logo_new.png" width="137.5" /><br><h3 style="display: inline">Order P.O.#: {{ $form_data['po'] }}</h3></td>
        </tr>
        <tr>
            <td>
                <table style="">
                  <tr>
                    <td style="width: 50%">
                      <table>
                        <tr>
                            <th colspan=2 style="text-align: left;"><h3>{{ $form_data['form_name'] }}<h3></th>
                        </tr>
                        <tr>
                          <td><b>Dealer:</b> </td>
                          <td>{{ $user->dealer->name }}</td>
                        </tr>
                        <tr>
                          <td><b>Ordered by:</b> </td>
                          <td>{{ $form_data['ordered_by'] }}</td>
                        </tr>
                        <tr>
                          <td><b>Email:</b> </td>
                          <td>{{ $form_data['email'] }}</td>
                        </tr>
                        <tr>
                          <td><b>Phone:</b> </td>
                          <td>{{ $user->dealer->phone }}</td>
                        </tr>
                      </table>
                    </td>
                    <td style="width: 50%">
                      <table>
                        <tr>
                            <th><h3>Shipping Address<h3></th>
                        </tr>
                        <tr>
                           <td>{{ $order['shipping']['street1'] }}<br>
                            @if($order['shipping']['street2']) 
                              {{ $order['shipping']['street2'] }} <br>
                            @endif
                            {{ $order['shipping']['city'] }} {{ $order['shipping']['state'] }}, {{ $order['shipping']['postal_code'] }}<br>
                            {{ $order['shipping']['country'] }}
                           </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr style="width: 100%">
                    
                    <td style="width: 50%">
                      <table>
                        <tr>
                          <th><h3>Vehicle Info<h3></th>
                        </tr>
                         @if($form_data['form'] == 'minivan')
                        <tr>
                          <td><b>Year:</b> </td>
                          <td>{{ $form_data['vehicle_year'] }}</td>
                        </tr>
                        @endif
                        <tr>
                          <td><b>Make/Model:</b> </td>
                          <td>
                              {{ $order['makeModel']['name'] or  'N/A' }}
                          </td>
                        </tr>
                        @if($form_data['form'] == 'ht')
                        <tr>
                          <td><b>Conversion:</b></td>
                          <td>{{ $order['conversionBy']['name'] or 'N/A' }} {{ $form_data['ht_conversion_other'] or '' }}</td>
                        </tr>
                        @endif
                        @if($form_data['form'] == 'special')
                        <tr>
                          <td><b>Side of Vehicle:</b></td>
                          <td>{{ $form_data['side'] }}</td>
                        </tr>
                        @endif
                         @if($form_data['form'] == 'minivan' || $form_data['form'] == 'fullsize')
                        <tr>
                          <td><b>Con. Company:</b> </td>
                          <td>{{ $order['conversionBy']['name'] }}</td>
                        </tr>
                        <tr>
                          <td><b>Con. Type:</b> </td>
                          <td>{{ $order['conversionType']['name'] }}</td>
                        </tr>
                        <tr>
                          <td><b>Con. Year:</b> </td>
                          <td>{{ $form_data['conversion_year'] }}</td>
                        </tr>
                        <tr>
                          <td><b>Ramp:</b> </td>
                          <td>{{ $form_data['ramp'] }}</td>
                        </tr>
                        @endif
                      </table>
                    </td>
                    <td style="width: 50%">
                      <table>
                        <tr>
                            <th><h3>Billing Address<h3></th>
                        </tr>
                        <tr>
                           <td>{{ $order['billing']['street1'] }}<br>
                            @if($order['billing']['street2']) 
                              {{ $order['billing']['street2'] }} <br>
                            @endif
                            {{ $order['billing']['city'] }} {{ $order['shipping']['state'] }}, {{ $order['shipping']['postal_code'] }}<br>
                            {{ $order['billing']['country'] }}
                           </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            @if($form_data['form'] == 'ht')
            <tr>
              <td>
                <table>
                  <tr style="width: 100%;">
                    <td colspan=2><h2 style="color: #25699C">Application Info</h2></td>
                  </tr>
                  <tr style="width: 100%;">
                    <td>
                        <tr>
                          <td><b>Wheelchair Make:</b> </td>
                          <td>{{ $order['wheelchair_make'] }}</td>
                        </tr>
                        <tr>
                          <td><b>Wheelchair Model:</b> </td>
                          <td>{{ $order['wheelchair_model'] }}</td>
                        </tr>
                        <tr>
                          <td><b>Application:</b> </td>
                          <td>{{ $order['application'] }}</td>
                        </tr>
                        <tr>
                          <td><b>The user will be:</b> </td>
                          <td>{{ $order['action'] }}</td>
                        </tr>
                        <tr>
                          <td><b>When docked, the unit will be located:</b> </td>
                          <td>{{ $order['located'] }}</td>
                        </tr>
                    </td>
                  </tr>
                  <tr style="width: 100%;">
                    <td colspan=2><h2 style="color: #25699C">Hightower</h2></td>
                  </tr>
                  <tr style="width: 100%;">
                    <td>
                        <tr>
                          <td><b>Bracket:</b> </td>
                          <td>{{ $order['bracket'] }}</td>
                        </tr>
                        <tr>
                          <td><b>Station:</b> </td>
                          <td>{{ $order['station'] }}</td>
                        </tr>
                        @if (isset($order['floor_mount']))
                        <tr>
                          <td><b>Floor Mount:</b> </td>
                          <td>{{ $order['floor_mount'] }}</td>
                        </tr>
                        @endif
                      </td>
                    </tr>
                </table>
              </td>
            </tr>
             @endif
             @if($form_data['form'] == 'minivan' || $form_data['form'] == 'fullsize')
            <tr>
              <td>
                <table>
                  <tr style="width: 100%;">
                    <td colspan=2><h2 style="color: #25699C">Products</h2></td>
                  </tr>
                  <tr style="width: 100%;">
                    @foreach($order['seatbases'] as $seatbase)
                    <td>
                      <h3>{{ $seatbase['info']['name']}}</h3>
                        <table>
                            @foreach($seatbase['accessories'] as $accessory)
                            <tr>
                              <td>
                                <b>{{ $accessory['info']['name'] }}:</b>
                                @foreach($accessory['options'] as $options)
                                  {{ $options['name'] }} |
                                @endforeach
                              </td>
                            </tr>
                          @endforeach
                        </table>
                      </td>
                    @endforeach
                  </tr>
                </table>
              </td>
            </tr> 
            @endif                  
           </td>
        </tr>
      </table>
    </body>
</html>