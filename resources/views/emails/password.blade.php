<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body style="text-align: center;">
		<img src="http://bdidealers.com/assets/images/b%26d_logo_new.png" width="200px"  />
		<h2 style="text-align: center;">Password Reset</h2>
		
		<div style="text-align: center;">
			To reset your password, complete this form:<br> {{ URL::to('password/reset', array($token)) }}.
			<br>
			This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes.
		</div>
	</body>
</html>
