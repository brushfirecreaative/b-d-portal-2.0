<!DOCTYPE html>
<html>
<head>
  <title>Print Certificate</title>
  <style>
  * {
  margin: 0;
  padding: 0; }

body {
  width: 1020px;
  height: 788px;
  font-family: "Georgia", serif;
  font-style: italic;
  font-size: 14px;
  text-align: center;
  background: url("/assets/images/BD-Certificate.jpg") no-repeat;
  background-size: contain;
  }

.certificate-background {
  position: absolute;
  z-index: -100;
  left: 0;
  top: 0; }

.certificate_wrapper {
  width: 1020px;
  height: 788px;
  text-align: center;
  position: absolute; }

.text {
  position: absolute;
  top: 220px;
  width: 100%; }
.text p {
  margin-bottom: 10px; }

.p1 {
  font-size: 1.5em;
  font-weight: 100; }

.name {
  font-size: 3em;
  font-weight: bold; }

.dealer_name {
  font-size: 2em;
  font-weight: bold; }

.verbage {
  font-size: 1.5em;
  margin-top: 20px; }

.john_date {
  position: absolute;
  top: 653px;
  left: 185px; }

.vern_date {
  position: absolute;
  top: 653px;
  left: 720px; }

@media print {
  @page {
    size: landscape; } }

</style>
</head>
<body>
  <div class="certificate_wrapper">
  <div class="text">
    <p class="p1">This is to certify that</p>
    <p class="name">{{ $cert_user->prop_name }}</p>
    <p class="dealer_name">{{ $corporate->name }} - {{ $dealer->name }}</p>
    <p class="verbage">Successfully completed training for the <br> {{ $certificate->verbage }} <br> Manufactured by:</p> 
  </div>
  <p class="john_date">{{ date('F j, Y', strtotime($progress->completed_on )) }}</p>
  <p class="vern_date">{{ date('F j, Y', strtotime($progress->completed_on )) }}</p>
  </div> 
</body>
</html>




