@extends('layouts.master')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <div class="well well-lg">
    <h1>Welcome to the</h1>
    <h2>{{ $test['name'] }}</h2>
    <p class="text-danger">Important! You are about to begin the Leadership 61 Electrical Installation Test. Once you begin the test you will not be able to navigate away from the the test. If you leave the test at any time during the training or test portions all work up to that point will be lost.</p>
    <h5>The testing process is as follows:</h5>
    <ul>
      <li>Section 1: Training Video</li>
      <li>Section 2: Test</li>
      <li>You must receive a score of 80% or higher to pass</li>
    </ul>
    <div class="row">
      <div class="col-sm-6">
        <a href="/tests/training/{{ $test['id'] }}" class="btn btn-lg btn-success">Begin</a>

        <a href="/certificate" class="btn btn-lg btn-danger">Cancel</a>
      </div>
    </div>
  </div>
</div>

@stop
