@extends('layouts.master')

@section('addToHeader')
<link href="/assets/css/rprogress.css" rel="stylesheet" >
@stop

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  @foreach($certificates as $certificate)
    <div class="well well-lg">
      <div class="row">
        @if(isset($certificate['expired']))
        <h3 class="text-danger"><i class="fa fa-exclamation-circle"></i> Expired</h3>
        @endif
        <div class="col-md-8">
          <h1>{{ $certificate['info']['name'] }}</h1>
        </div>
        @if($certificate['progress']['progress'])
          @if(isset($certificate['expired']))
          <div class="col-md-2">
            <div class="progress-radial progress-0">
              <div class="overlay"><p class="text-danger">Expired</p></div>
            </div>
          </div>
          @else
            <div class="col-md-2">
              <div class="progress-radial progress-{{ $certificate['progress']['progress'] }}">
                <div class="overlay">{{ $certificate['progress']['progress'] }}</div>
              </div>
            </div>
          @endif
          @if($certificate['progress']['progress'] == 100)
            @if(isset($certificate['expires']))
            <div class="col-md-2" style="padding-top:3%">
              <a class="btn btn-primary wait" href="certificate/{{ $user->id}}/print/{{ $certificate['info']['id'] }}">Print Certificate</a>
            </div>
              <div class="col-md-2" style="padding-top:3%">
                <a class="btn btn-success" href="/certificate/{{ $certificate['info']['id'] }}">Begin New Certificate</a>
              </div>
            @elseif(isset($certificate['expired']))
              <div class="col-md-2" style="padding-top:3%">
                <a class="btn btn-success" href="/certificate/{{ $certificate['info']['id'] }}">Retake Certificate</a>
              </div>
            @else
            <div class="col-md-2" style="padding-top:3%">
              <a class="btn btn-primary wait" href="certificate/{{ $user->id}}/print/{{ $certificate['info']['id'] }}">Print Certificate</a>
            </div>
            @endif
          @else
            <div class="col-md-2" style="padding-top:3%">
              <a class="btn btn-warning" href="/certificate/{{ $certificate['info']['id'] }}">Continue Certificate</a>
            </div>
          @endif
        @else
        <div class="col-md-2">
          <div class="progress-radial progress-0">
            <div class="overlay">0%</div>
          </div>
        </div>
        <div class="col-md-2" style="padding-top:3%">
          <a class="btn btn-success" href="/certificate/{{ $certificate['info']['id'] }}">Begin Certificate</a>
        </div>
        @endif
        </div>  
      </div>
  @endforeach
</div>
@stop
@section('addToFooter')

@stop
