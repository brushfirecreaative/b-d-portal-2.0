@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">All Users</h1>
    <div class="table-responsive">
      <table class="table table-striped">
        <tr>
          <th>Username</th>
          <th>Full Name</th>
          <th>Email</th>
          <th>Dealer</th>
          <th>Dealer UID</th>
          <th>Confirmed</th>
        </tr>
        @foreach($users as $u)
          <tr id="user-{{ $u->id }}">
            <td><a href="/user/{{ $u->id }}">{{ $u->username }}</a></td>
            <td>{{ $u->prop_name }}</td>
            <td>{{ $u->email }}</td>
            <td>{{ $u->dealer['name'] }}</td>
            <td>{{ $u->dealer['unique_id'] }}</td>
            <td id="confirmed">
              @if($u->confirmed)
                <i class="fa fa-check text-success"></i>
              @else
                <button type="button" class="btn btn-sm btn-warning confirmUser" data-toggle="modal"  data-id="{{ $u->id }}" data-target="#confirmUserModal" id="confirmUser"><i class="fa fa-exclamation-circle"></i> Confirm</button>
                <a class="btn btn-sm btn-danger" href="/user/{{ $u->id }}/unconfirm">Remove</a>
              @endif
            </td>
          </tr>
        @endforeach
      </table>
    </div>
@stop
@section('addToFooter')
<div class="modal fade" tabindex="-1" role="dialog" id="confirmUserModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirm User</h4>
      </div>
      <div class="modal-body">
          {{ Form::open(array('url' => '/user/confirm', 'method' => 'post', 'class' => 'form', 'id' => 'confirmForm')) }}
          <div>
          {{ Form::hidden('userId', '', array('id' => 'userId', 'class' => 'form-control')) }}
        </div>
        <p>To confirm this user, please select the level of access that best fits position or job title.</p>
        <div>
          {{ Form::select('positions', $positions, null, array('class' => 'form-control', 'id' => 'positions')) }}
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type-="submit"  class="btn btn-primary" id="confirmBtn">Confirm</button>
        {{ Form::close() }}
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
$('.confirmUser').click(function (e) {
    console.log('clicked');
     var userId = $(this).data('id');
     $(".modal-body #userId").val( userId );
});
</script>
<script>
$(document).ready(function() {
  $('#confirmForm').submit(function(event) {

    $('#confirmBtn').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...');
    var formData = {
      '_token' : $('input[name=_token').val(),
      'userId' : $('input[name=userId]').val(),
      'positions' : $('#positions').val()
    };
    $.ajax({
      type      : 'POST',
      url       : '/user/confirm/1',
      data      : formData,
      dataType  : 'json',
      encode    : true
    })
    .done(function(data) { 
      $('#confirmUserModal').modal('hide');
      $('#user-'+formData.userId).removeClass('warning');
      $('#user-'+formData.userId+' td#confirmed').html('<i class="fa fa-check text-success"></i>');
      $('#confirmBtn').html('Confirm');
    });

    event.preventDefault();
  });
});
</script>

@stop