@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    {{ Form::open(array('url' => '/search-corporate-users', 'method' => 'post', 'class' => 'form')) }}
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-addon"><span class="fa fa-search"></span></div>
            {{ Form::text('query', null, array( 'placeholder' => 'Search Users...', 'class' => 'form-control input-lg' )) }}
          </div>
        </div>
        {{ Form::hidden('corporate', $dealer->corporate->id) }}
        <div class="text-center">
          {{ Form::submit('Search', array('class' => 'btn btn-primary btn-lg')) }}
        </div>
      {{ Form::close() }}
    <h1 class="page-header">Search Results</h1>
    <a href="/corporate_users/{{ $dealer->corporate->id }}">All {{ $dealer->corporate->name }} Users</a>
    <div class="table-responsive">
      <table class="table table-striped">
        <tr>
          <th>Username</th>
          <th>Full Name</th>
          <th>Email</th>
          <th>Dealer</th>
        </tr>
        @if(empty($users))
          <h2>No Search Results</h2>
          <a href="/users">Return to all {{ $dealer->name }} users </a>
        @endif
        @foreach($users as $u)
          <tr>

            <td><a href="/user/{{ $u['id'] }}">{{ $u['username'] }}</a></td>
            <td>{{ $u['prop_name'] }}</td>
            <td>{{ $u['email'] }}</td>
            <td>{{ $u['dealer']['name'] }}</td>
          </tr>
        @endforeach
      </table>
    </div>
@stop