@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
      {{ Form::open(['route' => 'user.store', 'class' => 'form-horizontal']) }}
        <h3><i class="fa fa-user fa-fw"></i>New User</h3>
        {{-- <div class="form-group">
          <label class="col-sm-2"></i> <strong>Username:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('username', '', ['id' => 'username','class' => 'form-control']) }}
             {{ $errors->first('username', '<span class="text-danger">:message</span>') }}
          </div>
        </div> --}}
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Email:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('email', '', ['class' => 'form-control']) }}
             {{ $errors->first('email', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Full Name:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('prop_name', '', ['class' => 'form-control']) }}
             {{ $errors->first('prop_name', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         <div class="form-group">
          <label class="col-sm-2"></i> <strong>Dealer:</strong> </label>
          <div class="col-sm-6">

            {{ Form::select('dealer', $dealer_array, '', ['class' => 'form-control']) }}
             {{ $errors->first('dealer', '<span class="text-danger">:message</span>') }}
       </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Office Phone:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('phone', '', ['class' => 'form-control']) }}
               {{ $errors->first('phone', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Extension:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('extension', '', ['class' => 'form-control']) }}
             {{ $errors->first('extension', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Cell Phone:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('cell-phone', '', ['class' => 'form-control']) }}
             {{ $errors->first('cell-phone', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Fax:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('fax','', ['class' => 'form-control']) }}
               {{ $errors->first('fax', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Password:</strong> </label>
            <div class="col-sm-6">
              {{ Form::password('password', ['id' => 'password', 'class' => 'form-control']) }}
               {{ $errors->first('password', '<span class="text-danger">:message</span>') }}
            </div>
            <div class="col-sm-3">
             <div class="messages"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Password Confirm:</strong> </label>
            <div class="col-sm-6">
              {{ Form::password('password_confirmation', ['id' => 'password_confirm', 'class' => 'form-control']) }}
               {{ $errors->first('password_confirmation', '<span class="text-danger">:message</span>') }}
            </div>
            <div class="col-sm-3">
              <div id="validate-status"></div>
            </div>
        </div>
        <h3><i class="fa fa-unlock-alt"></i> User Roles</h3>
        <small>Pick all Roles you want this user to have</small>
        @foreach($roles as $role)
          <div class="checkbox">
            <label>
            {{ Form::checkbox('roles[]', $role['id']) }}
            {{ $role['name'] }}
            </label>
          </div>
        @endforeach
        {{ Form::hidden('position', 1) }}
        <div class="form-group">
          <div class="col-sm-6 col-sm-offset-2">
            {{ Form::submit('Add User', array('class' => 'btn btn-success')) }}
          </div>
        </div>
          
      {{ Form::close() }}
@stop

@section('addToFooter')
<script type="text/javascript" src="/assets/js/passwordStrength.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  $("#password_confirm").keyup(validate);
});


function validate() {
  var password = $("#password").val();
  var password_confirm = $("#password_confirm").val();

    
 
    if(password == password_confirm) {
       $("#validate-status").addClass('text-success').removeClass('text-danger').html("<i class='fa fa-check'></i> Passwords Match");        
    }
    else {
        $("#validate-status").addClass('text-danger').removeClass('text-success').html("<i class='fa fa-times'></i> Passwords Do Not Match");  
    }
    
}

</script>
@stop
