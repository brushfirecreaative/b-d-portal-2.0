@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <a href="/admin/users"><i class="fa fa-arrow-left"></i> Back to Users</a>
  <div class="jumbotron">
    <h1 class="page-header">{{ $u->prop_name }}</h1>
    <div class="row">
      <div class="col-md-6">
        <p>Dealership: {{ $u->dealer['name'] }}</p>
        <p>Corporate: {{ $u->dealer->corporate['name'] }}</p>
        <p><a class="btn btn-primary btn-lg" href="/user/{{ $u->id }}/edit">Edit Profile</a></p>
      </div>
      <div class="col-md-6">
        <p>Roles: 
        @foreach($u->roles as $role)
          {{ $role->name }} | 
        @endforeach 
      </p>
        <p>Email: {{ $u->email }}</p>
        <p>Phone: {{ $u->phone }}</p>
        <p>Fax: {{ $u->fax }}</p>
        <p>
      </div>
    </div>
  </div>
  </div>
@stop