@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <a href="/user/{{ $u->id }}"><i class="fa fa-arrow-left"></i> Back to User Profile</a>
  <h1>Edit <strong>{{ $u->prop_name }}'s</strong> Profile</h1>
      {{ Form::open(['route' => ['user.update', $u['id']],'method' => 'put' ,'class' => 'form-horizontal']) }}
      <h3><i class="fa fa-key fa fw"></i>Change Password</h3>
      <div class="form-group">
            <label class="col-sm-2"></i> <strong>Password:</strong> </label>
            <div class="col-sm-6">
              {{ Form::password('password', ['id' => 'password', 'class' => 'form-control']) }}
               {{ $errors->first('password', '<span class="text-danger">:message</span>') }}
            </div>
            <div class="col-sm-3">
             <div class="messages"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Password Confirm:</strong> </label>
            <div class="col-sm-6">
              {{ Form::password('password_confirmation', ['id' => 'password_confirm', 'class' => 'form-control']) }}
               {{ $errors->first('password_confirmation', '<span class="text-danger">:message</span>') }}
            </div>
            <div class="col-sm-3">
              <div id="validate-status"></div>
            </div>
        </div>
      <h3><i class="fa fa-user fa-fw"></i>User Information</h3>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-user fa-fw"></i> <strong>Username:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('username', $u['username'], ['class' => 'form-control']) }}
            {{ $errors->first('username', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-building-o fa-fw"></i> <strong>Dealer:</strong> </label>
          <div class="col-sm-6">
            {{ Form::select('dealer', $dealer_array, $u['dealer']['id'], ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-building fa-fw"></i> <strong>Parent Company:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('company' ,$u['dealer']['corporate']['name'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-star fa-fw"></i> <strong>Full Name:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('prop_name', $u['prop_name'], ['class' => 'form-control']) }}
            {{ $errors->first('prop_name', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-envelope fa-fw"></i> <strong>Email:</strong> </label>
          <div class="col-sm-6">
            {{ Form::email('email', $u['email'], ['class' => 'form-control']) }}
             {{ $errors->first('email', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-phone fa-fw"></i> <strong>Office Phone:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('phone', $u['phone'], ['class' => 'form-control']) }}
             {{ $errors->first('phone', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-phone fa-fw"></i> <strong>Extension:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('extension', $u['extension'], ['class' => 'form-control']) }}
             {{ $errors->first('extension', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-phone fa-fw"></i> <strong>Cell Phone:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('cell-phone', $u['cell_phone'], ['class' => 'form-control']) }}
             {{ $errors->first('cell-phone', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-fax fa-fw"></i> <strong>Fax:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('fax', $u['fax'], ['class' => 'form-control']) }}
             {{ $errors->first('fax', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <h3><i class="fa fa-credit-card fa-fw"></i>Billing Info</h3>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Address:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('street1', $u['dealer']['street1'], ['class' => 'form-control']) }}
             {{ $errors->first('street1', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>City:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('city', $u['dealer']['city'], ['class' => 'form-control']) }}
             {{ $errors->first('city', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         <div class="form-group">
          <label class="col-sm-2"></i> <strong>State:</strong> </label>
          <div class="col-sm-6">

            {{ Form::select('state', $state_arr, $u['dealer']['state'], ['class' => 'form-control']) }}
             {{ $errors->first('state', '<span class="text-danger">:message</span>') }}
       </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Postal Code:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('postal_code', $u['dealer']['postal_code'], ['class' => 'form-control']) }}
               {{ $errors->first('postal_code', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Country:</strong> </label>
            <div class="col-sm-6">
              {{ Form::select('country', $country_arr, $u['dealer']['country'], ['class' => 'form-control']) }}
               {{ $errors->first('country', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <h3><i class="fa fa-unlock-alt"></i> User Roles</h3>
        <small>Pick all Roles you want this user to have</small>
        @foreach($roles as $role)
          <div class="checkbox">
            <label>
            {{ Form::checkbox('roles[]', $role['id'], $role['checked']) }}
            {{ $role['name'] }}
            </label>
          </div>
        @endforeach
        <br><br>
        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
       <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".du-modal">Delete User</button>

      {{ Form::close() }}

      <h3><i class="fa fa-truck fa-fw"></i>Shipping Addresses</h3>
      <a href="shipping/create" class=" btn btn-success"><i class="fa fa-plus fa-fw"></i> New Shipping Address</a>
      <br><br>
      <div class="row">
        @foreach( $shipping_addresses as $shipping)
          <div class="col-md-3 col-sm-4 col-lg-2">
            
              @if ($shipping['default_address']) 
              <div class="panel panel-success">
                <div class="panel-heading "><i class="fa fa-check fa-2x fa-fw"></i><strong>Default</strong></div>
                @else
                  <div class="panel panel-warning">
                <div class="panel-heading "><a href="/shipping/make-default/{{ $shipping['id'] }}" class="btn btn-sm btn-primary">Make Default</a></div>
                @endif

              <div class="panel-body">
                {{ $shipping['street1'] }} <br>
                {{ $shipping['city'] }} {{ $shipping['state'] }}, {{ $shipping['postal_code'] }} <br>
                {{ $shipping['county'] }}
                
              </div>
            </div>
          </div>
        @endforeach
      
  </div>
  <div class="modal fade du-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Delete User</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete the user {{ $u['email'] }}?</p>
          <p>By Deleting this user you will delete all data associated with them as well, including:</p>
          <ul>
            <li>Certification Data</li>
            <li>Profile Data</li>
          </ul>
          <p>This data can not be recovered. If you would like to procced press "Delete User"</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a href="/user/{{ $u['id'] }}/delete" class="btn btn-danger">Delete User</a>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@stop
@section('addToFooter')
<script type="text/javascript" src="/assets/js/passwordStrength.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  $("#password_confirm").keyup(validate);

});


function validate() {
  var password = $("#password").val();
  var password_confirm = $("#password_confirm").val();

    
 
    if(password == password_confirm) {
       $("#validate-status").addClass('text-success').removeClass('text-danger').html("<i class='fa fa-check'></i> Passwords Match");        
    }
    else {
        $("#validate-status").addClass('text-danger').removeClass('text-success').html("<i class='fa fa-times'></i> Passwords Do Not Match");  
    }
    
}

</script>
@stop