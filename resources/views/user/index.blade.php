@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      {{ Form::open(['route' => ['user.update', $user['id']],'method' => 'put' ,'class' => 'form-horizontal']) }}
      <h3><i class="fa fa-key fa fw"></i>Change Password</h3>
      <div class="form-group">
            <label class="col-sm-2"></i> <strong>Password:</strong> </label>
            <div class="col-sm-6">
              {{ Form::password('password', ['id' => 'password', 'class' => 'form-control']) }}
               {{ $errors->first('password', '<span class="text-danger">:message</span>') }}
            </div>
            <div class="col-sm-3">
             <div class="messages"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Password Confirm:</strong> </label>
            <div class="col-sm-6">
              {{ Form::password('password_confirmation', ['id' => 'password_confirm', 'class' => 'form-control']) }}
               {{ $errors->first('password_confirmation', '<span class="text-danger">:message</span>') }}
            </div>
            <div class="col-sm-3">
              <div id="validate-status"></div>
            </div>
        </div>
      <h3><i class="fa fa-user fa-fw"></i>User Information</h3>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-user fa-fw"></i> <strong>Username:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('username', $user['username'], ['class' => 'form-control']) }}
            {{ $errors->first('username', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-building-o fa-fw"></i> <strong>Dealer:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('dealer', $user['dealer']['name'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-building fa-fw"></i> <strong>Parent Company:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('company', $user['dealer']['parent']['name'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-star fa-fw"></i> <strong>Full Name:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('prop_name', $user['prop_name'], ['class' => 'form-control']) }}
            {{ $errors->first('prop_name', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-envelope fa-fw"></i> <strong>Email:</strong> </label>
          <div class="col-sm-6">
            {{ Form::email('email', $user['email'], ['class' => 'form-control']) }}
             {{ $errors->first('email', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-phone fa-fw"></i> <strong>Office Phone:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('phone', $user['phone'], ['class' => 'form-control']) }}
             {{ $errors->first('phone', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-phone fa-fw"></i> <strong>Extension:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('extension', $user['extension'], ['class' => 'form-control']) }}
             {{ $errors->first('extension', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-phone fa-fw"></i> <strong>Cell Phone:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('cell-phone', $user['cell_phone'], ['class' => 'form-control']) }}
             {{ $errors->first('cell-phone', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><i class="fa fa-fax fa-fw"></i> <strong>Fax:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('fax', $user['fax'], ['class' => 'form-control']) }}
             {{ $errors->first('fax', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <h3><i class="fa fa-credit-card fa-fw"></i>Billing Info</h3>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Address:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('street1', $user['dealer']['street1'], ['class' => 'form-control']) }}
             {{ $errors->first('street1', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>City:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('city', $user['dealer']['city'], ['class' => 'form-control']) }}
             {{ $errors->first('city', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         <div class="form-group">
          <label class="col-sm-2"></i> <strong>State:</strong> </label>
          <div class="col-sm-6">

            {{ Form::select('state', $state_arr, $user['dealer']['state'], ['class' => 'form-control']) }}
             {{ $errors->first('state', '<span class="text-danger">:message</span>') }}
       </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Postal Code:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('postal_code', $user['dealer']['postal_code'], ['class' => 'form-control']) }}
               {{ $errors->first('postal_code', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Country:</strong> </label>
            <div class="col-sm-6">
              {{ Form::select('country', $country_arr, $user['dealer']['country'], ['class' => 'form-control']) }}
               {{ $errors->first('country', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
          <div class="col-sm-6 col-sm-offset-2">
            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
          </div>
        </div>
          
      {{ Form::close() }}

      <h3><i class="fa fa-truck fa-fw"></i>Shipping Addresses</h3>
      <a href="shipping/create" class=" btn btn-success"><i class="fa fa-plus fa-fw"></i> New Shipping Address</a>
      <br><br>
      <div class="row">
        @foreach( $shipping_addresses as $shipping)
          <div class="col-md-3 col-sm-4 col-lg-2">
            
              @if ($shipping['default_address']) 
              <div class="panel panel-success">
                <div class="panel-heading "><i class="fa fa-check fa-2x fa-fw"></i><strong>Default</strong></div>
                @else
                  <div class="panel panel-warning">
                <div class="panel-heading "><a href="/shipping/make-default/{{ $shipping['id'] }}" class="btn btn-xs btn-primary">Make Default</a><a href="/shipping/remove/{{ $shipping['id'] }}" class="btn btn-xs btn-danger">X</a></div>
                @endif

              <div class="panel-body">
                {{ $shipping['street1'] }} <br>
                {{ $shipping['city'] }} {{ $shipping['state'] }}, {{ $shipping['postal_code'] }} <br>
                {{ $shipping['county'] }}
                
              </div>
            </div>
          </div>
        @endforeach
      
  </div>
@stop
@section('addToFooter')
<script type="text/javascript" src="/assets/js/passwordStrength.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  $("#password_confirm").keyup(validate);
});


function validate() {
  var password = $("#password").val();
  var password_confirm = $("#password_confirm").val();

    
 
    if(password == password_confirm) {
       $("#validate-status").addClass('text-success').removeClass('text-danger').html("<i class='fa fa-check'></i> Passwords Match");        
    }
    else {
        $("#validate-status").addClass('text-danger').removeClass('text-success').html("<i class='fa fa-times'></i> Passwords Do Not Match");  
    }
    
}

</script>
@stop