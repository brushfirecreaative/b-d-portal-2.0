@extends('knowledgebase.layout')

@section('knowledgebase')
    <div class="knowledgebase">
      @foreach($cats as $cat)
        <div class="category">
          <h3>{{ $cat['name'] }}</h3>
            @foreach($cat['children'] as $child)
              <ul>
                <li><a href="knowledgebase/category/{{ $child->id }}">{{ $child['name'] }}</a></li>
              </ul>
            @endforeach
        </div>
      @endforeach

    </div>
@stop