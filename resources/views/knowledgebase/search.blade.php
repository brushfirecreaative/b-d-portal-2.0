@extends('knowledgebase.layout')

@section('knowledgebase')
<a href="/knowledgebase"><i class="glyphicon glyphicon-arrow-left"></i>&nbsp; Back to Dealer Tools</a>
    <div class="knowledgebase">
    <h3>Search Results</h3>
    @if(count($docs) > 0)
      @foreach($docs as $doc)
         <div class="row document">
              <div class="col-sm-3 preview">
                {{ $doc->preview }}
              </div>
              <div class="col-sm-9">
                <a href="{{ $doc->body }}"><h4>{{ $doc->title }}</h4></a>
              </div>
      @endforeach
    @else
      <h2>No Results found.</h2>
    @endif
    </div>

@stop