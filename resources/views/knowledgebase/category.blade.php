@extends('knowledgebase.layout')

@section('knowledgebase')
  <a href="/knowledgebase"><i class="glyphicon glyphicon-arrow-left"></i>&nbsp; Back to Dealer Tools</a>
  <div class="knowledgebase">
  <h1>{{ $category->name }}</h1>
    @foreach($documents as $doc)
      @if(isset($doc['docs']))
        <h3>{{ $doc['name'] }}</h3>
          @foreach($doc['docs'] as $d)
          <div class="row document">
            <div class="col-sm-3 preview">
              {{ $d->preview }}
            </div>
            <div class="col-sm-9">
              <a href="{{ $d->body }}"><h4>{{ $d->title }}</h4></a>
            </div>
          </div>
          @endforeach
        @else
          <div class="row document">
            <div class="col-sm-3 preview">
              {{ $doc->preview }}
            </div>
            <div class="col-sm-9">
              <a href="{{ $doc->body }}"><h4>{{ $doc->title }}</h4></a>
            </div>
          </div>
        @endif
    @endforeach
  </div>
@stop