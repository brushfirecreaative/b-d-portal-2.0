@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="text-center"> Dealer Tools </h1>
    <div class="search_form">
      {{ Form::open(array('url' => 'knowledgebase/search', 'method' => 'post', 'class' => 'form')) }}
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-addon"><span class="fa fa-search"></span></div>
            {{ Form::text('query', null, array( 'placeholder' => 'Search Dealer Tools...', 'class' => 'form-control input-lg' )) }}
          </div>
        </div>
        <div class="text-center">
          {{ Form::submit('Search', array('class' => 'btn btn-primary btn-lg')) }}
        </div>
      {{ Form::close() }}
    </div>
    @yield('knowledgebase')
  </div>
@stop