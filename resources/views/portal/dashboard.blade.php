@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Welcome, {{ $user['prop_name'] }}</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">

              <a href="/certificate"><i class="fa fa-certificate fa-fw fa-5x"></i><br> Take Certification</a>
            </div>
            @if($user->hasRoleOrHigher(3))
            <div class="col-xs-6 col-sm-3 placeholder">
              <a href="/order"><i class="fa fa-truck fa-fw fa-5x"> </i><br>Place Order</a>
            </div>
            @endif
            <div class="col-xs-6 col-sm-3 placeholder">
              <a href="/knowledgebase"><i class="fa fa-question fa-fw fa-5x"></i><br> Search Knowledgebase</a>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <a href="/myinfo"><i class="fa fa-gears fa-fw fa-5x"></i><br> Update My Info</a>
            </div>
          </div>

          <h2 class="sub-header">News</h2>
          <div class="table-responsive">
            
          </div>
        </div>
        
@stop