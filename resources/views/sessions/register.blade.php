@extends('layouts.login')

@section('content')
<div class="container">
  {{ Form::open(array('url' => 'register/check', 'class' => 'form-signin', 'role' => 'form')) }}
    <img src="/assets/images/bd_logo_new.png" width="100%"  />
    <div>
      <h2 class="form-signin-heading text-center">User Registration</h2>
      {{ Form::text('dealer_id', '', array('class' => 'form-control', 'placeholder' => 'Dealer ID', 'required', 'autofocus')) }}
    </div>
    <button class="btn btn-lg btn-success btn-block" type="submit">Begin Registration</button>
    <a class="text-center" href="/">Login</a>
  {{ Form::close() }}
</div>
  @stop