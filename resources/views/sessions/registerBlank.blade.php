@extends('layouts.login')

@section('content')
<div class="text-center">
  <h2 >{{ $message }}</h2>
  <a href="/">Back to Login...</a>
</div>
@stop