@extends('layouts.login')

@section('content')
<div class="contianer">
  {{ Form::open(array('url' => 'register/confirm', 'class' => 'form-signin', 'role' => 'form')) }}
    <img src="/assets/images/bd_logo_new.png" width="100%"  />
    <div>
      <h3 class="form-signin-heading text-center">Choose a Role for <i>{{ $confirming_user->username }}</i></h3>
      {{ Form::hidden('user', $confirming_user->id, array('class' => 'form-control')) }}
      <p> Select a Role for this user. They have inticated that their position is:</p>
      <h5><i><b>{{ $confirming_user->position }}</b></i></h5>
      {{ Form::select('role', $roles, null, array('class' => 'form-control')) }}
    </div>
    <br>
    <button class="btn btn-lg btn-success btn-block" type="submit">Confirm &amp; Save Role</button>
  {{ Form::close() }}
</div>
@stop