@extends('layouts.login')

@section('content')
	<div class="container">
	<h2 class="form-signin-heading text-center">Reset Password</h2>
		<form action="{{ action('RemindersController@postRemind') }}" method="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    <input type="email" name="email" class="form-control">
		    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Send Reminder">
		</form>
	</div>
@stop