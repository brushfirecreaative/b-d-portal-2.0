@extends('layouts.login')

@section('content')
<div class="container">
  
  {{ Form::open(array('route' => 'sessions.store', 'class' => 'form-signin', 'id' => 'loginForm', 'role' => 'form')) }}
    <img src="/assets/images/bd_logo_new.png" width="100%"  />
    <div>
      <!-- <h2 class="form-signin-heading text-center">Dealer Portal 2.0 Login</h2> -->
       <h3 class="text-center">Welcome to the B&D Independence Dealer Portal 2.0</h3>
      <div class="well text-center">
       
        <p>To proceed, please select the statement below that best describes you.</p>
        
        {{ Form::select('Iam', $iam, '',array('class' => 'form-control', 'id' => 'iam_select')) }}

        <div id="iam_div"></div>

        <!--
        <h5>I have a Technician login and Dealer login from the previous portal</h5>
        <p>Please proceed by logging in with your tech credentials</p>
         <h5>I have only a Dealer login</h5>
        <p>Please proceed by logging in with your dealer credentials, you will be directed to create a personal login</p>

        <h5>I have only a Dealer ID</h5>
        <p>Please proceed by clicking the Technician Registration button below, you will be directed to create a personal login</p>

        <h5>I have nothing</h5>
        <p>Please contact your Supervisor or B&D Independence to receive your Dealer ID</p> -->
      </div>
      @if ($error = $errors->first('password'))
        <div class="alert alert-danger">
        {{ $error }}
        </div>
      @endif
      @if ($error = $errors->first('confirmed'))
        <div class="alert alert-danger">
        {{ $error }}
        </div>
      @endif
    </div>
    <div id="login">
      {{ Form::text('username', isset($email) ? $email : '', array('id' => 'user', 'class' => 'form-control', 'placeholder' => 'Email', 'required', 'autofocus')) }}
      {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password', 'required')) }}
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <a class="" href="/forgot-password">Forgot Password</a>
    </div>
    <a class="text-center btn btn btn-success btn-block" id="registerBtn" href="register">Register</a>
    
  {{ Form::close() }}
</div>

  <div id="contact_form"  style="display:none;">
    {{ Form::open(array('url' => 'contact', 'class' => 'form-signin')) }}
      <h4>Contact B&amp;D</h4>
      {{ Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Your Name', 'required')) }}
      {{ Form::text('dealer', '', array('class' => 'form-control', 'placeholder' => 'Your Dealership Name', 'required')) }}
      {{ Form::text('dealer_info', '', array('class' => 'form-control', 'placeholder' => 'Your Dealership City and State', 'required')) }}
      {{ Form::text('email', isset($email) ? $email : '', array('id' => 'contact-email', 'class' => 'form-control', 'placeholder' => 'Your Email', 'required', 'autofocus')) }}
      {{ Form::text('phone', '', array('class' => 'form-control', 'placeholder' => 'Phone Number', 'required'))}}
    <button class="btn btn-lg btn-primary btn-block" type="submit">Send Email</button>
    {{ Form::close() }}
  </div>

  @stop


  @section('addToFooter')
  <script>
  $(document).ready( function() {
    $('#iam_select').change( function(){
      var value = $('#iam_select').val();
      var msg = '';
      var ph = 'Username';

      if(value === 'tech')
      {
        msg = 'Please proceed by logging in with your technician email and password.';
        ph = 'Technician Email';
        $('#login').show();
        $('#registerBtn').show();
        $('#contact_form').hide();
      }
      else if(value === 'dealer')
      {
         msg = 'Please proceed by logging in with your dealer credentials. You will be directed to create a personal account';
         ph = 'Dealer Username';
         $('#login').show();
         $('#registerBtn').show();
         $('#contact_form').hide();
      }
      else if(value === 'id')
      {
        msg = 'Please proceed by clicking the <i>Registration</i> button below, you will be directed to create a personal login';
        $('#login').hide();
        $('#registerBtn').show();
        $('#contact_form').hide();
      }
      else if(value === 'nothing')
      {
        msg = 'Please contact your Supervisor or B&D Independence to receive your Dealer ID to create a personal account.';
        $('#login').hide();
        $('#registerBtn').hide();
        $('#contact_form').show();
      }
      $('#user').attr('placeholder', ph);
      $('#iam_div').html('<p class="bg-info" style="padding: 1em; text-weight: bold;">' + msg + '</p>');
    });
  });
  
  </script>

  @stop