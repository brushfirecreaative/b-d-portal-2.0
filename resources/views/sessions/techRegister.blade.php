@extends('layouts.login')

@section('content')
<div class="col-md-7 col-md-offset-2">
  <div class="techs well clearfix">
    <h3>The following users have already created an account. If your email address is listed here, please proceed by using your login. If you do not see your email address listed, please create a new account.</h3>
    <ul class="list-unstyled row">
    @foreach($techs as $tech)

      <li class="col-md-4" style="margin: .5em 0;"><a href="/login/{{ urlencode($tech->email) }}" class="btn btn-default">{{ $tech->email }}</a></li>
    @endforeach
    </ul>
  </div>
</div>

  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    @if(isset($techs))
  @endif
      {{ Form::open(['url' => 'user/register', 'class' => 'form-horizontal']) }}
        <h3><i class="fa fa-user fa-fw"></i>Register new user for {{ $dealer->name }}</h3>
 <!--        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Username:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('username', '', ['id' => 'username','class' => 'form-control']) }}
             {{ $errors->first('username', '<span class="text-danger">:message</span>') }}
          </div>
        </div> -->
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Email:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('email', '', ['class' => 'form-control']) }}
             {{ $errors->first('email', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Full Name:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('prop_name', '', ['class' => 'form-control']) }}
             {{ $errors->first('prop_name', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Office Phone:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('phone', '', ['class' => 'form-control']) }}
               {{ $errors->first('phone', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Extension:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('extension', '', ['class' => 'form-control']) }}
             {{ $errors->first('extension', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"> <strong>Cell Phone:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('cell-phone', '', ['class' => 'form-control']) }}
             {{ $errors->first('cell-phone', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Fax:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('fax','', ['class' => 'form-control']) }}
               {{ $errors->first('fax', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Position:</strong> </label>
            <div class="col-sm-6">
              {{ Form::select('position', $positions, 0, ['class' => 'form-control', 'id' => 'positionSelect']) }}
               {{ $errors->first('position', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div id="positionOtherDiv" class="form-group" style="display: none;">
            <label class="col-sm-2"></i> <strong>Please describe your job title or what you would utilize this Dealer Portal for:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('position_other',  '', ['class' => 'form-control', 'id' => 'positionOther']) }}
               {{ $errors->first('position_other', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Password:</strong> </label>
            <div class="col-sm-6">
              {{ Form::password('password', ['id' => 'password', 'class' => 'form-control']) }}
               {{ $errors->first('password', '<span class="text-danger">:message</span>') }}
            </div>
            <div class="col-sm-3">
             <div class="messages"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Password Confirm:</strong> </label>
            <div class="col-sm-6">
              {{ Form::password('password_confirmation', ['id' => 'password_confirm', 'class' => 'form-control']) }}
               {{ $errors->first('password_confirmation', '<span class="text-danger">:message</span>') }}
            </div>
            <div class="col-sm-3">
              <div id="validate-status"></div>
            </div>
        </div>
        <div class="form-group">
          <div class="col-sm-6 col-sm-offset-2">
            {{ Form::hidden('dealer', $dealer->id) }}
            {{ Form::submit('Submit', array('class' => 'btn btn-success')) }}
          </div>
        </div>
          
      {{ Form::close() }}
@stop

@section('addToFooter')
<script type="text/javascript" src="/assets/js/passwordStrength.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  $("#password_confirm").keyup(validate);
});

$('#positionSelect').change( function() {
  console.log($('#positionSelect').val());
  if($('#positionSelect').val() == 'none')
  {
    $('#positionOtherDiv').show();
  }
  else
  {
     $('#positionOtherDiv').hide();
  }
})

function validate() {
  var password = $("#password").val();
  var password_confirm = $("#password_confirm").val();

    
 
    if(password == password_confirm) {
       $("#validate-status").addClass('text-success').removeClass('text-danger').html("<i class='fa fa-check'></i> Passwords Match");        
    }
    else {
        $("#validate-status").addClass('text-danger').removeClass('text-success').html("<i class='fa fa-times'></i> Passwords Do Not Match");  
    }
    
}

</script>
@stop
