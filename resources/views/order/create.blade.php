@extends('layouts.master')

@section('addToHeader');

@stop

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main {{ $bodyClass or 'validates'}}">
  {{ Form::open(array('url' => 'order/review', 'method' => 'post', 'class' => 'orderForm')) }}
    {{ Form::hidden('form', $form->view) }}
    {{ Form::hidden('form_id', $form->id, ['id' => 'form_id']) }}
    <h1>{{ $form->name }}</h1>
    <div class="row">
      <div class="well col-md-9 ">
        <h3><i class="fa fa-building"></i> Order Details</h3>
        <div class="form-group">
        {{-- Shipping Address --}}
          <em class="text-danger">*</em>
          {{ Form::label('shipping_address', 'Shipping Address') }}
          {{ Form::select('shipping_address', $shipping, $default_shipping->id, ['class' => 'form-control', 'required']) }}
          {{ $errors->first('shipping_address', '<span class="text-danger">:message</span>') }}
        </div>
        <div class="form-group">
        {{-- Billing Address --}}
          <em class="text-danger">*</em>
          {{ Form::label('billing_address', 'Billing Address') }}
          {{ Form::select('billing_address', $shipping, $default_shipping->id, ['class' => 'form-control', 'required']) }}
          {{ $errors->first('billing_address', '<span class="text-danger">:message</span>') }}
        </div>
        <div class="form-group">
       {{-- Ordered By --}}
          <em class="text-danger">*</em>
          {{ Form::label('ordered_by', 'Ordered By') }}
          {{ Form::text('ordered_by', $user->prop_name, ['class' => 'form-control', 'required']) }}
          {{ $errors->first('ordered_by', '<span class="text-danger">:message</span>') }}
        </div>
        <div class="form-group">
         {{-- Your Email --}}
            <em class="text-danger">*</em>
            {{ Form::label('email', 'Your Email') }}
            {{ Form::email('email', $user->email, ['class' => 'form-control', 'required']) }}
            {{ $errors->first('email', '<span class="text-danger">:message</span>') }}
        </div>
        <div class="form-group">
         {{--  P.O. --}}
            <em class="text-danger">*</em>
            {{ Form::label('po', 'P.O.') }}
            {{ Form::text('po', null, ['class' => 'form-control', 'required']) }}
            {{ $errors->first('po', '<span class="text-danger">:message</span>') }}
        </div>
      </div> 
    </div> {{-- End Row --}}
        @if($form->view == 'minivan')
          @include('order.minivanForm')
        @elseif($form->view == 'fullsize')
          @include('order.fullsizeForm')
        @elseif($form->view == 'special')
            @include('order.specialForm')
        @elseif($form->view == 'ht')
          @include('order.htForm')
        @endif
    <div class="seatbase row"></div>
    <div class="row">
      <div class="col-md-9">
        <div class="well">
          {{ Form::submit('Review Order', ['class' => 'btn btn-primary']) }}
          <a href="/order/" class="btn btn-danger">Cancel</a>
        </div>
      </div>
    </div>  
  {{ Form::close() }}  
</div>
@stop

@section('addToFooter')
<script src='https://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.min.js'></script>
<script src='/assets/js/orderForm.js'></script>
@stop