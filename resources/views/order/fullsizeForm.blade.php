    <div class="row">
      <div class="well col-md-9 ">
        <h3><i class="fa fa-truck"></i> Vehicle Information</h3>
        <div class="form-group">
       {{-- Vehicle Make/Model --}}
          <em class="text-danger">*</em>
          {{ Form::label('vehicle_make', 'Vehicle Make/Model') }}
          {{ Form::select('vehicle_make', $options['makeModel'], null, ['class' => 'form-control', 'id' => 'make_model', 'required']) }}
          {{ $errors->first('vehicle_make', '<span class="text-danger">:message</span>') }}
        </div>
      </div> 
    </div> {{-- End Row --}}
    <div class="row">
      <div class="well col-md-9 ">
        <h3><i class="fa fa-truck"></i> Conversion Information</h3>
        <div class="form-group">
        {{-- Conversion By --}}
          <em class="text-danger">*</em>
          {{ Form::label('conversion_by', 'Conversion By') }}
          {{ Form::select('conversion_by', array(), null, ['class' => 'form-control', 'id' => 'cb', 'required']) }}
          {{ $errors->first('conversion_by', '<span class="text-danger">:message</span>') }}
        </div>
        <div class="form-group">
        {{-- Conversion Type --}}
          <em class="text-danger">*</em>
          {{ Form::label('conversion_type', 'Conversion Type') }}
          {{ Form::select('conversion_type', array('' => 'Conversion type...'), null, ['class' => 'form-control', 'id' => 'ct', 'required']) }}
          {{ $errors->first('conversion_type', '<span class="text-danger">:message</span>') }}
        </div>
        <div class="form-group">
        {{-- Conversion Year' --}}
          <em class="text-danger">*</em>
          {{ Form::label('conversion_year', 'Conversion Year') }}
          {{ Form::text('conversion_year', null, ['class' => 'form-control', 'required']) }}
          {{ $errors->first('conversion_year', '<span class="text-danger">:message</span>') }}
        </div>
         <div class="form-group">
         {{-- Type of Ramp --}}
            <em class="text-danger">*</em>
            {{ Form::label('ramp', 'Type  of Ramp') }}
            {{ Form::select('ramp', $options['ramp'], null, ['class' => 'form-control', 'required']) }}
            {{ $errors->first('ramp', '<span class="text-danger">:message</span>') }}
        </div>
      </div> 
    </div> {{-- End Row --}}