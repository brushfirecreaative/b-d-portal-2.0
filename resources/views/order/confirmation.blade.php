@extends('layouts.master')

@section('addToHeader');

@stop

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
 	<div class="jumbotron text-center">
	    <h1 class="page-header">Thank you, <br>Your order has been placed.</h1>
	    <h2>You should receive an email copy of your order shortly.</h2>
	</div>
</div>
@stop

@section('addToFooter')

@stop