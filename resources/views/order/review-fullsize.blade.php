@extends('layouts.master')

@section('addToHeader');

@stop

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="text-right">
		{{ Form::open(array('url' => 'order', 'method' => 'post', 'class' => 'orderForm')) }}
 		{{ Form::submit('Submit Order', ['class' => 'btn btn-primary']) }}
 		{{ Form::close() }}
 	</div>
 	<div class="jumbotron">
	    <h1 class="page-header">Review Order</h1>
	    <h1>P.O.# : {{ $form_data['po'] }}</h1>
	    <div class="row">
	    	<div class="col-md-4">
	        	<p>Dealer: {{ $user->dealer->name }} </p>
	        	<p>Ordered by: {{ $form_data['ordered_by'] }} </p>
	        	<p>Email: {{ $form_data['email'] }}</p>
	        	<p>Phone: {{ $user->dealer->phone }} </p>
	    	</div>
	    	<div class="col-md-4">
	    		<p><strong>Shipping Address</strong></p>
	        	<p>{{ $order['shipping']['street1'] }} </p>
	        	@if($order['shipping']['street2']) 
	        		<p>{{ $order['shipping']['street2'] }} </p>
	        	@endif
	        	<p>{{ $order['shipping']['city'] }} {{ $order['shipping']['state'] }}, {{ $order['shipping']['postal_code'] }}</p>
	        	<p>{{ $order['shipping']['country'] }}</p>
	     	</div>
	     	<div class="col-md-4">
	        	<p><strong>Vehicle Info</strong></p>
	        	<p>Make/Model: {{ $order['makeModel']['name'] }}</p>
	        	<p>Conversion By: {{ $order['conversionBy']['name'] }}</p>
	        	<p>Conversion Type: {{ $order['conversionType']['name'] }}</p>
	        	<p>Conversion Year: {{ $form_data['conversion_year'] }}</p>
	        	<p>Ramp: {{ $form_data['ramp'] }}</p>
	     	</div>
	    </div>
	    <h2>Products</h2>
	    <div class="row">
	    	@foreach($order['seatbases'] as $seatbase)
	    		<div class="col-sm-12">
	    			<img src="/assets/media/{{$seatbase['info']['media']}}" width="100" class="pull-left" /> <h3>{{ $seatbase['info']['name']}}</h3>
	    			<div class="row">
	    				<div class="col-sm-9 col-sm-offset-3">
	    					@foreach($seatbase['accessories'] as $accessory)
	    						<p><b>{{ $accessory['info']['name'] }}:</b>
	    							@foreach($accessory['options'] as $options)
	    								{{ $options['name'] }} |
	    							@endforeach
	    						</p>
	    					@endforeach
	    				</div>
	    			</div>
	    		</div>
	    	@endforeach
	    </div>
	</div>
	<div class="text-right">
		{{ Form::open(array('url' => 'order', 'method' => 'post', 'class' => 'orderForm')) }}
 		{{ Form::submit('Submit Order', ['class' => 'btn btn-primary']) }}
 		{{ Form::close() }}
 	</div>
</div>
@stop

@section('addToFooter')

@stop