<h4>Options and Accessories</h4>

@foreach ($accessories as $accessory)
@if($accessory->standard == 'true')
  <?php $type = 'radio'; 
    $required = "required";
    ?>

@else
   <?php $type = 'checkbox'; 
   $required = NULL;
   ?>
@endif
<div class="well">
  <h5>{{ $accessory->name }}</h5>
    @foreach ($accessory->options as $option)
        <div class="{{$type}}">
          <label>
          {{ Form::$type('options['.$product.']['. $accessory->id .'][]', $option->id, false, [$required]) }}
          {{ $option->name }} ({{ $option->price }})
          </label>
        </div>
    @endforeach
  </div>
@endforeach