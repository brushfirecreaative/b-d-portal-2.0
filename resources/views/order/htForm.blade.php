<div class="row">
  <div class="well col-md-9 ">
    <h3><i class="fa fa-wheelchair"></i> Wheelchair Information</h3>
    <div class="form-group">
   {{-- Wheelchair Make --}}
      <em class="text-danger">*</em>
      {{ Form::label('wheelchair_make', 'Wheelchair Make') }}
      {{ Form::select('wheelchair_make', $options['wc_make'], null, ['class' => 'form-control', 'id' => 'ht_make', 'required']) }}
      {{ $errors->first('wheelchair_make', '<span class="text-danger">:message</span>') }}
    </div>
    <div class="form-group">
   {{-- Wheelchair Model --}}
      <em class="text-danger">*</em>
      {{ Form::label('wheelchair_model', 'Wheelchair Model') }}
      {{ Form::select('wheelchair_model', array(), null, ['class' => 'form-control', 'id' => 'ht_model', 'required']) }}
      {{ $errors->first('wheelchair_model', '<span class="text-danger">:message</span>') }}
    </div>
  </div> 
</div> {{-- End Row --}}
<div class="row">
  <div class="well col-md-9 ">
    <h3><i class="fa fa-arrows"></i> Application Information</h3>
    <div class="form-group">
   {{-- Application 1 --}}
      <em class="text-danger">*</em>
      {{ Form::label('application', 'Application') }}
      {{ Form::select('application', array(), null, ['class' => 'form-control', 'id' => 'ht_ap', 'required']) }}
      {{ $errors->first('application', '<span class="text-danger">:message</span>') }}
    </div>
    <div class="form-group hidden" id="ht_action_div">
   {{-- User will be --}}
      <em class="text-danger">*</em>
      {{ Form::label('ht_action', 'The user will be:') }}
      {{ Form::select('ht_action', $options['ht_action'], null, ['class' => 'form-control', 'id' => 'ht_action', 'required']) }}
      {{ $errors->first('ht_action', '<span class="text-danger">:message</span>') }}
    </div>
    <div class="form-group hidden" id="ht_located_div">
   {{-- Located--}}
      <em class="text-danger">*</em>
      {{ Form::label('ht_located', 'When docked, the unit will be located:') }}
      {{ Form::select('ht_located', $options['ht_located'], null, ['class' => 'form-control', 'id' => 'ht_located', 'required']) }}
      {{ $errors->first('ht_located', '<span class="text-danger">:message</span>') }}
    </div>
    <div class="form-group hidden" id="ht_vehicle_div">
   {{-- Vehicle--}}
      <em class="text-danger">*</em>
      {{ Form::label('ht_vehicle', 'This is a Chrysler or Dodge Minivan:') }}
      {{ Form::select('ht_vehicle', array(0 => '', 13 => 'Yes', 14 => 'No' ), null, ['class' => 'form-control', 'id' => 'ht_vehicle', 'required']) }}
    </div>
    <div class="form-group hidden" id="ht_conversion_div">
   {{-- Conversion--}}
      <em class="text-danger">*</em>
      {{ Form::label('ht_conversion', 'What is the Vehicle Conversion?') }}
      {{ Form::select('ht_conversion', $options['conversion'], null, ['class' => 'form-control', 'id' => 'ht_conversion', 'required']) }}
      {{ $errors->first('ht_conversion', '<span class="text-danger">:message</span>') }}
    </div>
    <div class="form-group hidden" id="ht_conversion_other_div">
         {{--  Other Conversion --}}
            {{ Form::label('ht_conversion_other', 'Please write a short description of the conversion company and type.') }}
            {{ Form::text('ht_conversion_other', null, ['class' => 'form-control', 'required']) }}
    </div>
    <div class="form-group">
            <button class="btn btn-success" id="get_config">Get My HighTower Setup</button>
    </div>
    <div class="col-md-6 hidden" id="hightower">
      <h3>Your Hightower </h3>
      <div id="hightower">
        <h4>Bracket:<span id="ht_bracket"></span></h4>
        <h5>Station:<span id="ht_station"><span></h5>
        <h5>Floor Mount Kit:<span id="ht_floor_mount"></span></h5>
      </div>
  </div> 
  <div class="col-md-6 hidden" id="graphic">
    <div id="ht_graphic"></div>
  </div>

</div> {{-- End Row --}}