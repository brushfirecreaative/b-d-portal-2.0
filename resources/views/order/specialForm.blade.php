    <div class="row">
      <div class="well col-md-9 ">
        <h3><i class="fa fa-truck"></i> Vehicle Information</h3>
        <div class="form-group">
          {{-- Vehicle Make/Model --}}
          <em class="text-danger">*</em>
          {{ Form::label('vehicle_make', 'Vehicle Make/Model') }}
          {{ Form::select('vehicle_make', $options['makeModel'], null, ['class' => 'form-control', 'id' => 'make_model_special', 'required']) }}
          {{ $errors->first('vehicle_make', '<span class="text-danger">:message</span>') }}
        </div>
        <div class="form-group">
          {{-- Side --}}
          <em class="text-danger">*</em>
          {{ Form::label('side', 'Side') }}
          {{ Form::select('side', array('Driver' => 'Driver', 'Passenger' => 'Passenger'), null, ['class' => 'form-control', 'id' => 'cb', 'required']) }}
          {{ $errors->first('side', '<span class="text-danger">:message</span>') }}
        </div>
      </div> 
    </div> {{-- End Row --}}