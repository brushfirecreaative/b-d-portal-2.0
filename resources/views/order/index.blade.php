@extends('layouts.master')

@section('addToHeader');

@stop

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <div class="row">
    @foreach($forms as $form)
    <div class="col-sm-12 text-left well">
      <h2 >{{ $form->name }}<h2>
      <a href="/order/{{ $form->id }}/new" class="btn btn-success">Begin Order</a>  
    </div>
    @endforeach
  </div>
</div>
@stop

@section('addToFooter')

@stop