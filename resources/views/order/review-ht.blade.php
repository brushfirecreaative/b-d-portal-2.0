@extends('layouts.master')

@section('addToHeader');

@stop

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="text-right">
		{{ Form::open(array('url' => 'order', 'method' => 'post', 'class' => 'orderForm')) }}
 		{{ Form::submit('Submit Order', ['class' => 'btn btn-primary']) }}
 		{{ Form::close() }}
 	</div>
 	<div class="jumbotron">
	    <h1 class="page-header">Review Order</h1>
	    <h1>P.O.# : {{ $form_data['po'] }}</h1>
	    <div class="row">
	    	<div class="col-md-6">
	        	<p>Dealer: {{ $user->dealer->name }} </p>
	        	<p>Ordered by: {{ $form_data['ordered_by'] }} </p>
	        	<p>Email: {{ $form_data['email'] }}</p>
	        	<p>Phone: {{ $user->dealer->phone }} </p>
	    	</div>
	    	<div class="col-md-6">
	    		<p><strong>Shipping Address</strong></p>
	        	<p>{{ $order['shipping']['street1'] }} </p>
	        	@if($order['shipping']['street2']) 
	        		<p>{{ $order['shipping']['street2'] }} </p>
	        	@endif
	        	<p>{{ $order['shipping']['city'] }} {{ $order['shipping']['state'] }}, {{ $order['shipping']['postal_code'] }}</p>
	        	<p>{{ $order['shipping']['country'] }}</p>
	     	</div>
	    </div>
	    <div class="row">
	     	<div class="col-md-6">
	     		<p><strong>WheelChair Info</strong></p>
	     		<p><b>Wheelchair Make:</b> {{ $form_data['wheelchair_make'] }}</p>
	     		<p><b>Wheelchair Model:</b> {{ $form_data['wheelchair_model'] }}</p>
	     	</div>
	     	<div class="col-md-6">
	        	<p><strong>Vehicle Info</strong></p>
	        	<p><b>Make/Model:</b> {{ $order['makeModel']['name'] or 'N/A' }}</p>
	        	<p><b>Conversion By:</b> {{ $order['conversionBy']['name'] or 'N/A' }} {{ $form_data['ht_conversion_other'] or '' }}</p>
	        	<p><b>Application:</b> {{ $order['application'] }}</p>
	        	<p><b>The user will be:</b> {{ $order['action'] or 'N/A' }}</p>
	        	<p><b>When docked, the unit will be located:</b> {{ $order['located'] or 'N/A' }}</p>
	     	</div>
	    </div>
	    <h2>Products</h2>
	    <div class="row">
	    	<div class="col-md-12">
	    		<h3><i>Bracket:</i> {{ $order['bracket'] }}</h3>
	    		<h3><i>Station:</i> {{ $order['station'] }}</h3>
	    		@if (isset($order['floor_mount']))
	    			<h3><h3><i>Floor Mount Kit:</i> {{ $order['floor_mount'] }}</h3>
	    		@endif;
	    	</div>
	    </div>
	</div>
	<div class="text-right">
		{{ Form::open(array('url' => 'order', 'method' => 'post', 'class' => 'orderForm')) }}
 		{{ Form::submit('Submit Order', ['class' => 'btn btn-primary']) }}
 		{{ Form::close() }}
 	</div>
</div>
@stop

@section('addToFooter')

@stop