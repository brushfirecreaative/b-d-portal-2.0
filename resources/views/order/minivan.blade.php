<div class="well col-md-9">
      <h3>Comfort Series Seat Bases</h3>
      <div id="seatbase">
        <h4><strong>{{ $series->name }}</strong> <em>{{ $series->description }}</em></h4>
        <h5><em>*</em> Choose Seat Base</h5>
        @foreach ($products as $product)
        <div class="panel panel-primary ">
          <div class="panel-heading"> <h5>{{ $product->name }}</h5></div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-6">
                  <div class="checkbox">
                    <label>
                      {{ Form::checkbox('seatbase[]', $product->id, null, ['class' => 'seatCheck']) }}
                      {{ $product->name }}
                     </label>
                   
                  </div>
                </div>
                <div class="col-md-6">
                  <img src='/assets/media/{{ $product->media }}' width='50%' />
                </div>
              </div>
              <div class="accessories-{{ $product->id }}"></div>
            </div>
          </div>
        </div>
        
        <br>
        @endforeach
</div>
<script>
$('.seatCheck').each(function() {
    $(this).change(function() {
    var id = $(this).val();
    console.log(id);
    var _token = $("input[name=_token]").val();
    if ( this.checked) {
      
      $.ajax({
        type: "POST",
        url:  "/order/getAccessories", 
        data: { id: id, _token: _token }
      })
        .done(function( data ) {

          $(".accessories-" + id).html(data);
            
        });
    }
    else
    {
      $(".accessories-" + id).html(' ');
    }
  });
});

</script>