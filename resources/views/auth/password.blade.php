@extends('layouts.login')

@section('content')
<div class="container">
	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="container">
		
		<form action="{{ url('/password/email') }}" method="POST" class="form-signin">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<img src="/assets/images/bd_logo_new.png" width="100%"  />
			<div>
				<h2 class="form-signin-heading text-center">Reset Password</h2>
		    	<input value="{{ old('email') }}"  type="email" name="email" class="form-control" placeholder="Email Address">
		   </div>
		    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Send Reminder">
		     <a href="/"><i class="fa fa-arrow-left fa-fw"></i> Back to Login</a>
		</form>
	</div>
</div>

@endsection
