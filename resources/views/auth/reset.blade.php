@extends('layouts.login')

@section('content')
<div class="container">

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<form action="{{ url('/password/reset') }}" method="POST" class="form-signin">
		<img src="/assets/images/bd_logo_new.png" width="100%"  />
		<h2 class="form-signin-heading text-center">Reset Password</h2>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="token" value="{{ $token }}">
	    <div>
	    	<input value="{{ old('email') }}" type="email" name="email" class="form-control" placeholder="Email">
	    </div>
	    <div>
	    	<input type="password" name="password" class="form-control" placeholder="Password">
	    </div>
	    <div>
	    	<input type="password" name="password_confirmation" class="form-control" placeholder="Password Confirm">
	    </div>
	    <input type="submit" value="Reset Password" class="btn btn-lg btn-primary btn-block" >
	    <a href="/"><i class="fa fa-arrow-left fa-fw"></i> Back to Login</a>
	</form>

</div>
@endsection
