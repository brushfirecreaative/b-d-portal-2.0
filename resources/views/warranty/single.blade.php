@extends('layouts.master')

@section('content')
 
  <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-2 main">
    <h2><i class="fa fa-certificate fa-fw"></i>Warranty Information</h2>
      @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
      @endif
      {{ Form::open(['url' => ['admin/warranty/update/'.$warranty->id],'method' => 'post' ,'class' => 'form-horizontal']) }}
      <h3>Product Information</h3>
      <div class="form-group">
          <label class="col-sm-2"><strong>Date:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('date', $warranty->date, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Serial Number:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('serial_number', $warranty->serial_number, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Model Number:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('model_number', $warranty->model_number, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Product:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('product', $warranty->product, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Dealer Name:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('dealer', $warranty->dealer, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Dealer City:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('dealer_city', $warranty->dealer_city, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Dealer State:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('dealer_state', $warranty->dealer_state, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Dealer Zip:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('dealer_zip', $warranty->dealer_zip, ['class' => 'form-control']) }}
          </div>
        </div>
        <h3>Owner Information</h3>
        <div class="form-group">
          <label class="col-sm-2"><strong>First Name:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('first_name', $warranty->first_name, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Last Name:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('last_name', $warranty->last_name, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Address:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('street', $warranty->street, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>City:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('city', $warranty->city, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>State:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('state', $warranty->state, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Zip:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('zip', $warranty->zip, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Phone:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('phone', $warranty->phone, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"><strong>Email:</strong></label>
          <div class="col-sm-6">
            {{ Form::text('email', $warranty->email, ['class' => 'form-control']) }}
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-6 col-sm-offset-2">
            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
          </div>
        </div>
          
      {{ Form::close() }}
      
  </div>
  <div class="col-sm-3 main">
    <h2>Notes</h2>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#warrantyNote">Add Note</button>
    <br><br>
    @foreach($warranty->notes as $note)
    <div class="panel panel-default">
      <div class="panel-body">
        <p> {{$note->note}}</p>
        <i>Date: {{$note->date}}</i> | <i>Made By: {{$note->user->prop_name}}</i>
      </div>
    </div>
    @endforeach
  </div>
@stop
@section('addToFooter')
<div class="modal fade" id="warrantyNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add a Note</h4>
      </div>
      <div class="modal-body">
        {{ Form::open(['url' => ['admin/warranty/note/new/'.$warranty->id],'method' => 'post' ,'class' => 'form-horizontal']) }}
        <div class="form-group">
          <label class="col-sm-2"><strong>Note:</strong></label>
          <div class="col-sm-6">
            {{ Form::textarea('note', '', ['class' => 'form-control']) }}
          </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
@stop