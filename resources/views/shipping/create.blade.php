@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      {{ Form::open(['route' => 'shipping.store', 'class' => 'form-horizontal']) }}
        <h3><i class="fa fa-truck fa-fw"></i>New Shipping Info</h3>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Address:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('street1', '', ['class' => 'form-control']) }}
             {{ $errors->first('street1', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>City:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('city', '', ['class' => 'form-control']) }}
             {{ $errors->first('city', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         <div class="form-group">
          <label class="col-sm-2"></i> <strong>State:</strong> </label>
          <div class="col-sm-6">

            {{ Form::select('state', $state_arr, '', ['class' => 'form-control']) }}
             {{ $errors->first('state', '<span class="text-danger">:message</span>') }}
       </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Postal Code:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('postal_code', '', ['class' => 'form-control']) }}
               {{ $errors->first('postal_code', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Country:</strong> </label>
            <div class="col-sm-6">
              {{ Form::select('country', $country_arr, '', ['class' => 'form-control']) }}
               {{ $errors->first('country', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
              <label>
                <input name="default" value="1" type="checkbox"> Default Address
              </label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-6 col-sm-offset-2">
            {{ Form::submit('Add Address', array('class' => 'btn btn-success')) }}
          </div>
        </div>
          
      {{ Form::close() }}
@stop