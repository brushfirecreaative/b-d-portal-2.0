@extends('layouts.master')

@section('addToHeader');

@stop

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<h1>{{ $corporation->name }} Technicians</h1>
  <a href="/dealer/{{ $dealer->corporate->id }}/technicians" class="btn btn-info pull-right"><i class="fa fa-building"></i> My Dealer Technicians</a>
  <uL>
  @foreach($techs as $tech)
    <li><a href="#{{ $tech->id }}">{{ $tech->prop_name }}</a></li>
  @endforeach
  </uL>
<h1>Technician Details</h1>
  @foreach($techs as $tech)
    <div id="{{ $tech->id }}"  class="well">
      <h2><i class="fa fa-user"></i> {{ $tech->prop_name }} <small>{{ $tech->dealer->name }}</small></h2>
      @if(count($tech->results) == 0)
        <p class="text-center text-warning"><i class="fa fa-exclamation-circle"></i> This Technician has not started any Certificates:</p>
      @endif
      @foreach ($tech->results as $cert)
        <div class="cert-block">
          <div class="row">
            <div class="col-sm-6">
              <h4>{{ $cert->name }}</h4>
            </div>
            <div class="col-sm-6">
              @if($cert->progress == 100)
              <a href="/certificate/{{ $tech->id }}/print/{{ $cert->certificate_id }}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-print"></i> Print Certificate</a>
                <h4 class="text-success">Certificate Completed <?php echo date('m-d-Y', strtotime($cert->updated_at)); ?>! <i class="fa fa-graduation-cap fa-2x"></i></h4>
              @else
                <p>{{ $cert->progress }}%</p> 
              @endif
            </div>
          </div>
        </div>
      @endforeach
    </div>
  @endforeach
</div>
@stop

@section('addToFooter')

@stop