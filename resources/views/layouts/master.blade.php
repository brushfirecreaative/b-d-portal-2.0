<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>B&amp;D Indepence Dealer Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
   <link href="/assets/css/portal.css" rel="stylesheet">
   <link href="/assets/css/main.css" rel="stylesheet">
   <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  @yield('addToHeader')
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>

  <div class="navbar navbar-fixed-top" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <i class="fa fa-bars fa-2x"></i>
        </button>
        <a class="navbar-brand" href="#"><img src="/assets/images/logo.png" /> B&amp;D Independence </a>
        <h4 class="dealer_id_header pull-right">Your Dealer ID is: {{ $dealer->unique_id }}</h4>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="/logout"><i class="fa fa-minus fa-fw"></i>Logout</a></li>
        </ul>
        {{ Form::open(array('url' => 'knowledgebase/search', 'method' => 'post', 'class' => 'navbar-form navbar-right')) }}
          {{ Form::text('query', null, array( 'placeholder' => 'Search Dealer Tools...', 'class' => 'form-control ' )) }}
      {{ Form::close() }}
        <!-- <form class="navbar-form navbar-right">
          <input type="text" class="form-control" placeholder="Search...">
        </form> -->
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-3 col-md-2 sidebar">
        <ul class="nav nav-sidebar">
        <li class="parent_menu"><a href="/portal"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
          <li class="parent_menu"><a href="/myinfo"><i class="fa fa-gears fa-fw"></i> {{ $user['prop_name'] }}</a></li>
          <li class="parent_menu"><a href="/certificate"><i class="fa fa-certificate fa-fw"></i> Certification</a></li>
          @if($user->hasRoleOrHigher(3))
            <li class="parent_menu"><a href="/order"><i class="fa fa-truck fa-fw"> </i> Order</a></li>
            <!-- <li class="parent_menu"><a href="#"><i class="fa fa-list fa-fw"></i> Order History</a></li> -->
            <li class="parent_menu"><a href="/dealer/{{ $user->dealer_id }}/technicians"><i class="fa fa-group fa-fw"></i> My Techs</a></li>
          @endif
          
          <li class="parent_menu"><a href="/knowledgebase"><i class="fa fa-question fa-fw"></i> Knowledgebase</a></li>
          @if($user->hasRoleOrHigher(3))
          <li class="parent_menu"><a href="/users"><i class="fa fa-users fa-fw"></i> Users</a><li>
          @endif
         @if($user->hasRoleOrHigher(5))
           <li class="parent_menu dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-cog fa-spin fa-fw"></i> Admin</a>
            <ul class="dropdown-menu">
              <li><a href="/admin/user/create"><i class="fa fa-plus fa-fw"></i> New User</a></li>
              <li><a href="/admin/dealer/create"><i class="fa fa-plus fa-fw"></i> New Dealer</a></li>
              <li><a href="/admin/users"><i class="fa fa-users fa-fw"></i> Users</a></li>
              <li><a href="/admin/users/unconfirmed"><i class="fa fa-times fa-fw"></i> Unconfirmed Users</a></li>
              <li><a href="/admin/dealers"><i class="fa fa-building fa-fw"></i> Dealers</a></li>
              <li><a href="/admin/warranty"><i class="fa fa-certificate fa-fw"></i> Warranty</a></li>
            </ul>
          </li>
        @endif
        </ul>
      </div>
        @yield('content')
    </div>
  </div>
  {{-- Flash Message --}}
  @if(\Session::has('message'))
    <div class="alert alert-info alert-dismissible alert-message">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        {{ \Session::get('message') }}
    </div>
@endif
  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <script src="/assets/js/bootstrap.min.js"></script>
    @yield('addToFooter')
    <script>
    $(document).ready(function() {
      $(".alert-message").fadeIn(500).css('display', 'block').delay(5000).fadeOut(500);

    });
    </script>
    <script>
      $(function () {
         setNavigation();
      });
      function setNavigation() {
        var path = window.location.pathname;
        path = path.replace(/\/$/, "");
        path = decodeURIComponent(path);

        $(".nav a").each(function () {
          var href = $(this).attr('href');
          if(path.substring(0,href.length) === href){
            $(this).closest('li.parent_menu').addClass('active');
          }
        });
      }
    </script>
  </body>
</html>