@extends('layouts.certificate')

@section('content')

<div class="col-md-8 col-md-offset-2">
  <div class="well text-center">
    @if($passfail == 'pass')
      <h2 class="text-success"><i class="fa fa-check"></i>Congratulations you have passed!</h2>
      <h1>Score: {{ $score }}%</h1>
      <br><br>
      @if($next_test)
        <a href="/certificate/{{ $certificate }}" class="btn btn-success">Next Test</a>
      @else
        <a href="/certificate/{{ $user->id }}/print/{{ $certificate }}" class="btn btn-success">Print Certificate</a>
      @endif
      <a href="/certificate" class="btn btn-primary">Done</a>
    @else
      <h2 class="text-danger"><i class="fa fa-exclamation-triangle"></i>Your score was not high enough to pass!</h2>
      <em class="text-primary"><i class="fa fa-info-circle"></i>You must score an 80% or better</em>
      <h1>Score: {{ $score }}%</h1>
      <br><br>
      <a href="/certificate/{{ $certificate }}" class="btn btn-success">Retake Test</a>
      <a href="/certificate" class="btn btn-danger">Cancel</a>
    @endif
  </div>
</div>
@stop
