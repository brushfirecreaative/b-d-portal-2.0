@extends('layouts.certificate')

@section('content')

<div class="videoContainer">
  <!-- Video -->
  <video id="training" controls preload="auto" poster="/assets/media/<?=$test['src']?>.jpg" >
      <source src="/assets/media/{{ $test['src'] }}.webm" type="video/webm">
      <source src="/assets/media/{{ $test['src'] }}.ogv" type="video/ogv">
      <source src="/assets/media/{{ $test['src'] }}.mp4" type="video/mp4">
      <p>
        Your browser doesn't support HTML5 video.
      </p>
  </video>
  <div class="test_actions" style="display:none;">
    <a href="/certificate" class="btn btn-danger btn-large"><i class="icon-remove-circle icon-white"></i> Back</a>
    <a href="/tests/{{ $test['id'] }}" class="btn btn-success btn-large">Take the Test <i class="icon-circle-arrow-right icon-white"></i></a>
  </div>
</div>

@stop

@section('add2Footer')
<script>
    $('video#training').bind('ended',function(){
        $('.test_actions').show('show');       
    });
</script>
@stop