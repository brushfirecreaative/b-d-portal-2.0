@extends('layouts.certificate')

@section('content')

<div class="col-md-8 col-md-offset-2">
<h1>{{ $test['name'] }} Test</h1>
{{ Form::open(['url' => '/tests/'.$test['id'], 'method' => 'PUT']) }}
<? $count = 1; ?>
  @foreach($questions as $question)
    <div class="question well">
      {{ $count }}. {{ $question['content'] }}
      @foreach($question['anwsers'] as $anwser)
      <div class="radio">
        <label>
         {{ Form::radio($anwser['question_id'], $anwser['id']) }}
         {{ $anwser['anwser'] }}
        </label>
      </div>
      @endforeach
    </div>
    <? $count++; ?>
  @endforeach
  <div class="well">
    {{ Form::submit('Submit Test', array('class' => 'btn btn-primary')) }}
    <a href="/certificate" class="confirm btn btn-danger">Cancel</a>
  </div>
</div>
{{ Form::close() }}
@stop
