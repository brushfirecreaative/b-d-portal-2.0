@extends('layouts.master')

@section('addToHeader');

@stop

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  {{ Form::open(array('url' => '/search-techs', 'method' => 'post', 'class' => 'form')) }}
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-addon"><span class="fa fa-search"></span></div>
            {{ Form::text('query', null, array( 'placeholder' => 'Search Technicians...', 'class' => 'form-control input-lg' )) }}
          </div>
        </div>
        {{ Form::hidden('dealer', $user->dealer_id) }}
        <div class="text-center">
          {{ Form::submit('Search', array('class' => 'btn btn-primary btn-lg')) }}
        </div>
      {{ Form::close() }}
<h1>{{ $dealer->name }} Technicians</h1>
  <a href="/corporate/{{ $dealer->corporate->id }}/technicians" class="btn btn-info pull-right"><i class="fa fa-building"></i> All Technicians in Corporation</a>
  <uL>
  @foreach($techs as $tech)
    <li><a href="#{{ $tech->id }}">{{ $tech->prop_name }}</a></li>
  @endforeach
  </uL>
<h1>Technician Details</h1>
  <div class="tech_list">
    @foreach($techs as $tech)
      <div id="{{ $tech->id }}"  class="well">
        <h2><i class="fa fa-user"></i> {{ $tech->prop_name }} <span style="font-size: 14px">({{ $tech->username }})</span></h2>
        @if(count($tech->results) == 0)
          <p class="text-center text-warning"><i class="fa fa-exclamation-circle"></i> This Technician has not started any Certificates:</p>
        @endif
        @foreach ($tech->results as $cert)
        @if($cert->expired)
          <div class="cert-block bg-danger" style="border: 1px solid #dca7a7; background: #f2dede;">
        @else
           <div class="cert-block">
        @endif
            <div class="row">
              <div class="col-sm-6">
                <h4>{{ $cert->name }}</h4>
              </div>
              <div class="col-sm-6">
                @if($cert->progress == 100 && !$cert->expired)
                <a href="/certificate/{{ $tech->id }}/print/{{ $cert->certificate_id }}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-print"></i> Print Certificate</a>
                  <h4 class="text-success">Certificate Completed <?php echo date('m-d-Y', strtotime($cert->completed_on)); ?>! <i class="fa fa-graduation-cap"></i></h4>
                @elseif($cert->expired)
                 <h4 class="text-danger">Certificate Exipred <i class="fa fa-exclamation-circle"></i></h4>
                @else
                  <p>{{ $cert->progress }}%</p> 
                @endif
              </div>
            </div>
          </div>
        @endforeach
      </div>
    @endforeach
  </div>
</div>
@stop

@section('addToFooter')


@stop