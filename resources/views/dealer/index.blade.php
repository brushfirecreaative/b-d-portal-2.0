@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">All Dealers</h1>
    <div class="table-responsive">
      <table class="table table-striped">
        <tr>
          <th>Dealer Name</th>
          <th>Phone</th>
          <th>Corporate</th>
        </tr>
        @foreach($dealers as $d)
          <tr>
            <td><a href="/dealer/{{ $d->id }}">{{ $d->name }}</a></td>
            <td>{{ $d->phone }}</td>
            <td>{{ $d->corporate['name'] }}</td>
          </tr>
        @endforeach
      </table>
    </div>
@stop
