<h3><i class="fa fa-building-o fa-fw"></i>New Corporate</h3>
<div class="form-group">
          <label class="col-sm-2"></i> <strong>Corporate Name:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('corporate_name', '', ['id' => 'corporate_name','class' => 'form-control']) }}
             {{ $errors->first('corporate_name', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Same Address as Dealer:</strong> </label>
          <div class="col-sm-6">
            {{ Form::checkbox('sameAddress', '1', NULL, ['id' => 'sameAddress']) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Phone:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('phone-c', '', ['id' => 'phone-c', 'class' => 'form-control']) }}
             {{ $errors->first('phone-c', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Fax:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('fax-c', '', ['id' => 'fax-c', 'class' => 'form-control']) }}
             {{ $errors->first('fax-c', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Url:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('url-c', '', ['id' => 'url-c', 'class' => 'form-control']) }}
               {{ $errors->first('url-c', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Street:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('street-c','', ['id' => 'street-c','class' => 'form-control']) }}
               {{ $errors->first('street-c', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Street 2:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('street2-c','', ['id' => 'street2-c', 'class' => 'form-control']) }}
               {{ $errors->first('street2-c', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Zip Code:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('zip-c', '',['id' => 'zip-c', 'class' => 'form-control']) }}
               {{ $errors->first('zip-c', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>City:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('city-c','',['id' => 'city-c', 'class' => 'form-control']) }}
               {{ $errors->first('city-c', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>State:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('state-c', '',['id' => 'state-c', 'class' => 'form-control']) }}
               {{ $errors->first('state-c', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Country:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('country-c', '',['id' => 'country-c', 'class' => 'form-control']) }}
               {{ $errors->first('country-c', '<span class="text-danger">:message</span>') }}
            </div>
        </div>