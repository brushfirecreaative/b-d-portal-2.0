@extends('layouts.master')

@section('content')
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <a href="/admin/dealers"><i class="fa fa-arrow-left"></i> Back to Dealers</a>
  <div class="jumbotron">
    <h1 class="page-header">{{ $d->name }}</h1>
    <div class="row">
      <div class="col-md-6">
        <p>UID: {{ $d->unique_id }}</p>
        <p>Corporate: {{ $d->corporate['name'] }}</p>
        <p><a class="btn btn-primary btn-lg" href="/dealer/{{ $d->id }}/edit">Edit Profile</a></p>
      </div>
      <div class="col-md-6">
        <p>Address: </p>
        <p>{{ $d->street1 }} @if($dealer->street != '')<br/> {{ $d->street2 }}@endif <br/> {{ $d->city }}, {{ $d->state }} {{ $d->postal_code }} <br/> {{ $d->country }}</p>
        <p>Phone: {{ $d->phone }}</p>
        <p>Fax: {{ $d->fax }}</p>
        <p>
      </div>
    </div>
    
  </div>
  <div class="">
    <h3>Users</h3>
    <div class="table-responsive">
      <table class="table table-striped">
        <tr>
          <th>Username</th>
          <th>Full Name</th>
          <th>Email</th>
          <th>Dealer</th>
          <th>Dealer UID</th>
        </tr>
        @foreach($users as $u)
          <tr>
            <td><a href="/user/{{ $u->id }}">{{ $u->username }}</a></td>
            <td>{{ $u->prop_name }}</td>
            <td>{{ $u->email }}</td>
            <td>{{ $u->dealer['name'] }}</td>
            <td>{{ $u->dealer['unique_id'] }}</td>
          </tr>
        @endforeach
      </table>
    </div>
  </div>
@stop