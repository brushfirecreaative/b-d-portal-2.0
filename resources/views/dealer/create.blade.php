@extends('layouts.master')

@section('content')

  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      {{ Form::open(['route' => 'dealer.store', 'class' => 'form-horizontal']) }}
        <h3><i class="fa fa-building fa-fw"></i>New Dealer</h3>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Dealer Name:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('dealer_name', '', ['id' => 'dealer_name','class' => 'form-control']) }}
             {{ $errors->first('dealer_name', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Phone:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('phone', '', ['id' => 'phone', 'class' => 'form-control']) }}
             {{ $errors->first('phone', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Fax:</strong> </label>
          <div class="col-sm-6">
            {{ Form::text('fax', '', ['id' => 'fax', 'class' => 'form-control']) }}
             {{ $errors->first('fax', '<span class="text-danger">:message</span>') }}
          </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Url:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('url', '', ['id' => 'url', 'class' => 'form-control']) }}
               {{ $errors->first('url', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>Street:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('street','', ['id' => 'street', 'class' => 'form-control']) }}
               {{ $errors->first('street', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Street 2:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('street2','', ['id' => 'street2', 'class' => 'form-control']) }}
               {{ $errors->first('street2', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Zip Code:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('zip', '',['id' => 'zip', 'class' => 'form-control']) }}
               {{ $errors->first('zip', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>City:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('city','',['id' => 'city', 'class' => 'form-control']) }}
               {{ $errors->first('city', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2"></i> <strong>State:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('state', '',['id' => 'state', 'class' => 'form-control']) }}
               {{ $errors->first('state', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2"></i> <strong>Country:</strong> </label>
            <div class="col-sm-6">
              {{ Form::text('country', '',['id' => 'country', 'class' => 'form-control']) }}
               {{ $errors->first('country', '<span class="text-danger">:message</span>') }}
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2"></i> <strong>Corporate:</strong> </label>
          <div class="col-sm-6">

            {{ Form::select('corporate', $corporates_array, '', ['id' => 'corporate', 'class' => 'form-control']) }}
             {{ $errors->first('corporate', '<span class="text-danger">:message</span>') }}
       </div>
        </div>
        <div id="addCorporate">
          @include('dealer.createCorporate-partial')
        </div>
        <div class="form-group">
          <div class="col-sm-6 col-sm-offset-2">
            {{ Form::submit('Add Dealer', array('class' => 'btn btn-success')) }}
          </div>
        </div>
          
      {{ Form::close() }}
@stop

@section('addToFooter')
<script src="/assets/js/jquery.ziptastic.js"></script>

<script>
$(document).ready(function(){
   $('#addCorporate').hide();
  $('#corporate').change(function(){
    var corporate = $('#corporate').val();
    if(corporate == 0)
    {
      $('#addCorporate').show(500);
    }
    else{
      $('#addCorporate').hide(500);
    }
  });
  $('#sameAddress').change(function() {
    if(this.checked) {
      var address = {
        phone: $('#phone').val(),
        fax: $('#fax').val(),
        url: $('#url').val(),
        street: $('#street').val(),
        street2: $('#street2').val(),
        zip: $('#zip').val(),
        city: $('#city').val(),
        state: $('#state').val(),
        country: $('#country').val()
      }

      $('#phone-c').val(address.phone);
      $('#fax-c').val(address.fax);
      $('#url-c').val(address.url);
      $('#street-c').val(address.street);
      $('#street2-c').val(address.street2);
      $('#zip-c').val(address.zip);
      $('#city-c').val(address.city);
      $('#state-c').val(address.state);
      $('#country-c').val(address.country);

    }
  });
});

  (function($) {
    $(function() {
        var duration = 500;
        var elements = {
            country: $('#country'),
            state: $('#state'),
            city: $('#city'),
            zip: $('#zip')
        }
        // Initially hide the city/state/zip
        // elements.country.parent().hide();
        // elements.state.parent().hide();
        // elements.city.parent().hide();
        // Initialize the ziptastic and bind to the change of zip code
        elements.zip.ziptastic()
          .on('zipChange', function(event, country, state, state_short, city, zip) {
             // Country
            elements.country.val(country).parent().show(duration);
            // State
            elements.state.val(state_short).parent().show(duration);
            // City
            elements.city.val(city).parent().show(duration);
          });
    });
  }(jQuery));
  (function($) {
    $(function() {
        var duration = 500;
        var elements = {
            country: $('#country-c'),
            state: $('#state-c'),
            city: $('#city-c'),
            zip: $('#zip-c')
        }
        // Initially hide the city/state/zip
        // elements.country.parent().hide();
        // elements.state.parent().hide();
        // elements.city.parent().hide();
        // Initialize the ziptastic and bind to the change of zip code
        elements.zip.ziptastic()
          .on('zipChange', function(event, country, state, state_short, city, zip) {
             // Country
            elements.country.val(country).parent().show(duration);
            // State
            elements.state.val(state_short).parent().show(duration);
            // City
            elements.city.val(city).parent().show(duration);
          });
    });
  }(jQuery));
</script>
@stop
