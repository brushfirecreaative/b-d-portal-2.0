<?php namespace Bdiportal;

class Result extends \Eloquent {
	protected $fillable = ['user_id, test_id, score, attempts'];
}