<?php

namespace Bdiportal;

use Illuminate\Database\Eloquent\Model;
use Bdiportal\MakeModel;

class Station extends Model
{

	protected $table = 'station';

    public static function ht_station($ap, $vehicle = null, $side = null)
	{
		if($vehicle)
		{
			$station = Station::where('application', $ap)->where('vehicle', $vehicle)->first();
			return $station->name;
		}
		else
		{
			$station = Station::where('application', $ap)->where('side', $side)->first();
			return $station->name;
		}

	}
}
