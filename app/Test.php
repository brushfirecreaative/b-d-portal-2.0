<?php namespace Bdiportal;
use Bdiportal\Certificate;
use Bdiportal\Question;

class Test extends \Eloquent {
	protected $fillable = [];

  public function certificate()
  {
    return $this->belongsTo('Bdiportal\Certificate');
  }

  public function questions()
  {
    return $this->hasMany('Bdiportal\Question');
  }
}