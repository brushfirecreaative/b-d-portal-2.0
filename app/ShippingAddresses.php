<?php namespace Bdiportal;

use Bdiportal\ShippingAddresses;
class ShippingAddresses extends \Eloquent {

  protected $table = 'shipping_addresses';


  public static function userShipping($id = NULL)
  {
    if($id == NULL) return ShippingAddresses::where('user_id', '=', auth()->user()->id)->get();

    return ShippingAddresses::where('user_id', '=', $id)->get();
    
  }

   public static function defaultAddress()
  {
    return ShippingAddresses::where('user_id', '=', \Auth::id())->where('default_address', '=', 1)->first();
  }

}
