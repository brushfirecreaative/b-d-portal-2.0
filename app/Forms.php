<?php namespace Bdiportal;

class Forms extends \Eloquent {
	protected $fillable = [];

  protected $table = 'forms';

  public function fields()
  {
    return $this->belongsToMany('Biportal\Field', 'form_field', 'form_id', 'field_id');
  }

}