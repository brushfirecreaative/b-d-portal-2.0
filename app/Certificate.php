<?php namespace Bdiportal;
use DB;
use Auth;
use Bdiportal\Test;

class Certificate extends \Eloquent {
	protected $fillable = [];

  public static function userProgress($cert)
  {
    
      return DB::table('progress')->where('user_id', '=', Auth::id())->where('certificate_id', '=', $cert)->first();
  }
  public static function getUserProgress($cert, $user)
  {
    
      return DB::table('progress')->where('user_id', '=', $user)->where('certificate_id', '=', $cert)->first();
  }

  public function tests()
  {
    return $this->hasMany('Bdiportal\Test');
  }

  public static function allProgress($user)
  {
    $progress = DB::table('progress')->where('user_id', '=', $user)->get();
    foreach($progress as $p)
    {
      $cert = Certificate::find($p->certificate_id);
      $p->name = $cert->name;
    }
    return $progress;
  }

}