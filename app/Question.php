<?php namespace Bdiportal;
use Bdiportal\Answer;

class Question extends \Eloquent {
	protected $fillable = [];

  public function anwsers()
  {
    return $this->hasMany('Bdiportal\Answer');
  }
}