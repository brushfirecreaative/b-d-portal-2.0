<?php

namespace Bdiportal;

use Illuminate\Database\Eloquent\Model;

class LegacyTechs extends Model
{
    protected $table = 'legacy_techs';
}
