<?php namespace Bdiportal;

class Category extends \Eloquent {
	protected $fillable = [];

  protected $table = 'categories';

  public function documents()
  {
    return $this->hasMany('Bdiportal\Document');
  }

  public function scopeOrdered($query)
{
    return $query->orderBy('id', 'asc')->get();
}

  public function children()
  {
    return $this->hasMany('Bdiportal\Category', 'parent', 'id');
  }
}