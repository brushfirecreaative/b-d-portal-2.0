<?php

namespace Bdiportal;

use Illuminate\Database\Eloquent\Model;

class Brackets extends Model
{
    public static function get_bracket($wc_make, $wc_model, $ap)
	{
		$bracket = Brackets::where('manufacturer', $wc_make)
			->where('series', $wc_model)
			->where('bracket_variation', $ap)
			->first();
			 //dd($bracket);
		return $bracket->set;
		
	}
}
