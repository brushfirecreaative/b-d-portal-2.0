<?php namespace Bdiportal;

class Accessory extends \Eloquent {
	protected $fillable = [];

  public function options()
  {
    return $this->belongsToMany('Bdiportal\Option');
  }
}