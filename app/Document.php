<?php namespace Bdiportal;

class Document extends \Eloquent {
	protected $fillable = [];

  public function category()
  {
    return $this->hasOne('Bdiportal\Category');
  }

  public static function getPreview($doc)
  {
    $type = substr($doc->body, -3);
      switch ($type):
        case 'jpg':
        case 'png':
        case 'gif':
        case 'tif':
         return  '<img src="'.$doc->body.'" />';
          break;
        case 'pdf':
          return '<span class="fa fa-file-pdf-o"></span>';
          break;
        case 'eps':
          return '<span class="fa fa-file-image-o"></span>';
          break; 
        case 'zip':
         return '<span class="fa fa-file-archive-o"></span>';
         break;
        default:
          return '<iframe width="100%" height="200" frameborder="0" allowfullscreen="" src="'.$doc->body .'"></iframe> ';
      endswitch;
  }
}