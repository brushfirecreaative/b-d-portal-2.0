<?php namespace Bdiportal;

class Dealer extends \Eloquent {
	protected $fillable = [];

  public function corporate()
  {
    return $this->belongsTo('Bdiportal\Corporate', 'corporate_id', 'id');
  }


  public function users()
  {
   return $this->hasMany('Bdiportal\User');
  }

  public static function supervisors($dealer)
  {
    $supervisors = array();
    $users = Dealer::find($dealer)->users;
    foreach($users as $user)
        {
          if($user->hasRole('supervisor')) $supervisors[] = $user->email;
        }
    return $supervisors;
  }

  public static function getByName($dealer_name){
      dd($dealer_name);
      $dealer = \DB::table('dealers')->where('name', '=', $dealer_name)->first();
      return $dealer->id;
  }

  public static function checkID($unique_id)
  {
      return \DB::table('dealers')->where('unique_id', '=', $unique_id)->first();


  }
}