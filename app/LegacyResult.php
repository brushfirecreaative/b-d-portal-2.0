<?php

namespace Bdiportal;

use Illuminate\Database\Eloquent\Model;

class LegacyResult extends Model
{
    protected $table = 'legacy_results';
}
