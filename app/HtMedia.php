<?php

namespace Bdiportal;

use Illuminate\Database\Eloquent\Model;
use Bdiportal\HtMedia;

class HtMedia extends Model
{
   protected $table = 'ht_media';

   public static function media($ap, $action = null, $side)
   {
   		$result = HtMedia::where('application', $ap)
   			->where('action', $action)
   			->where('side', $side)
   			->first();

   		if($result) return $result->media;

   		return null;

   		
   }
}
