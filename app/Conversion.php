<?php namespace Bdiportal;

use Bdiportal\Conversion;

class Conversion extends \Eloquent {
	protected $fillable = [];

   public static function selects()
  {
    foreach(Conversion::all() as $option)
    {
      $selects[$option->id] = $option->name;
    }

    return $selects;
  }
}