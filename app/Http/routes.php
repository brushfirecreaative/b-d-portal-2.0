<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('', 'SessionsController@create');
Route::get('forgot-password', 'Auth\PasswordController@getEmail');
// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::post('contact', 'ContactController@store');
// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
// Route::post('send-password', 'RemindersController@store');
// Route::get('password/reset/{token}', 'RemindersController@getReset');
// Route::post('password/reset', 'RemindersController@postReset');
Route::get('login', 'SessionsController@create');
Route::get('login/{email}', 'SessionsController@create');
Route::get('logout', 'SessionsController@destroy');
Route::resource('sessions', 'SessionsController');
Route::get('register', 'UserController@register');
Route::get('register/{id}', 'UserController@register');
Route::post('register/check', 'UserController@checkDealer');
Route::post('register/send', 'UserController@sendRegistation');
Route::post('user/register', 'UserController@store');
Route::get('auth/login', 'SessionsController@create');

Route::post('warranty-card', 'WarrantyController@store');

//Filter group for routes that need authentication to view 
Route::group(['middleware' => 'auth'], function()
{
  
  Route::get('myinfo', 'UserController@index');
  Route::get('portal', 'PortalController@dashboard');
  Route::resource('user', 'UserController');
  Route::resource('shipping', 'ShippingController');
  Route::get('shipping/make-default/{id}', 'ShippingController@makeDefault');
  Route::get('shipping/remove/{id}', 'ShippingController@destroy');
  Route::get('knowledgebase/category/{id}', 'KnowledgebaseController@category');

    

  // Technicians Only Routes
    
    // Route::group(['middleware' => ['role:tech']], function(){
    Route::resource('certificate', 'CertificateController');
    Route::get('certificate/{user}/print/{id}', 'CertificateController@print_certificate');
    Route::get('tests/training/{id}', 'TestsController@training');

    Entrust::routeNeedsRole('certificate', 'tech');
    Entrust::routeNeedsRole('certificate/*', 'tech');
    Entrust::routeNeedsRole('tests/*', 'tech');
    
    //Don't Show before Taking the Test
    Route::group(['middleware' => 'takingTest'], function()
    {
        Route::get('tests/completed', 'TestsController@completed');
        Route::get('tests/{id}', 'TestsController@show');
        Route::put('tests/{id}', 'TestsController@update');

  });
  // });
  //Supervisor only Routes
    Route::post('order/conversions', 'OrderController@conversions');
    Route::post('order/minivans', 'OrderController@minivans');
    Route::post('order/makeModel', 'OrderController@makeModel');
    Route::post('order/conversionBy', 'OrderController@conversionBy');
    Route::post('order/getAccessories', 'OrderController@accessories');
    Route::post('order/wc_models', 'OrderController@wc_models');
    Route::post('order/get_ht', 'OrderController@get_ht');
    Route::post('order/review', 'OrderController@reviewOrder');
    Route::get('order/{id}/new', 'OrderController@create');
    Route::resource('order', 'OrderController');
    Route::get('/users', 'UserController@dealerAll');
    Route::get('/user/{id}/delete', 'UserController@destroy');
    Route::get('/user/{id}/unconfirm', 'UserController@destroy');
    Route::post('/user/confirm',  'UserController@confirmUser');
    Route::post('/user/confirm/{isAjax}',  'UserController@confirmUser');
    Route::get('/corporate_users/{id}', 'UserController@corporateAll');
    Route::post('/search-techs', 'DealerController@techSearch');
    Route::post('/search-dealer-users', 'UserController@dealerSearch');
    Route::post('/search-corporate-users', 'UserController@corporateSearch');
    Route::get('register/confirm/{id}', 'UserController@confirmRegistration');
    Route::post('register/confirm/', 'UserController@storeRegistration');
    
    Entrust::routeNeedsRole('order', 'supervisor');
    Entrust::routeNeedsRole('order/*', 'supervisor');
    Entrust::routeNeedsRole('users/', 'supervisor');


  
  Route::get('dealer/{id}/technicians', 'DealerController@technicians');
  Route::get('corporate/{id}/technicians', 'CorporateController@technicians');
  Route::resource('knowledgebase', 'KnowledgebaseController');
  Route::post('knowledgebase/search', array(
    'as' => 'knowledgebase.search',
    'uses' => 'KnowledgebaseController@docSearch'
    )
  );
  

  //Admin Only Routes
     
    Route::get('admin/warranty', 'WarrantyController@index');
    Route::get('admin/warranty/{id}', 'WarrantyController@show');
    Route::post('admin/warranty/search', 'WarrantyController@search');
    Route::post('admin/warranty/update/{id}', 'WarrantyController@update');
    Route::post('admin/warranty/note/new/{warranty_id}', 'NoteController@store');
    Route::get('admin/users', 'UserController@listAll');
    Route::get('admin/users/unconfirmed', 'UserController@allUnconfirmed');
    Route::get('admin/dealers', 'DealerController@index');
    Route::resource('admin', 'AdminController');
    Route::get('admin/user/create', 'UserController@create');
    Route::get('admin/dealer/create', 'DealerController@create');
    Route::get('admin/user/{id}', 'UserController@show');
    Route::resource('dealer', 'DealerController');



    Entrust::routeNeedsRole('admin/*', 'admin');
    Entrust::routeNeedsRole('admin/users/*', 'admin');
    Entrust::routeNeedsRole('admin/user/*', 'admin');
    Entrust::routeNeedsRole('admin/dealer/*', 'admin');
    Entrust::routeNeedsRole('dealer', 'admin');
    Entrust::routeNeedsRole('dealer/*', 'admin');
  
});


