<?php namespace Bdiportal\Http\Middleware;

use Closure;
use Cookie;
use Redirect;
use Illuminate\Contracts\Auth\Guard;

class TakingTest{

	public function handle($request, Closure $next)
	{
		if(!Cookie::get('takingTest'))
			{
				return Redirect::to('/certificate');
			}	
		return $next($request);
	}

}
