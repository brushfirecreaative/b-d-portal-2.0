<?php namespace Bdiportal\Http\Controllers;
use Bdiportal\Certificate;
use Bdiportal\Test;
use Bdiportal\User;
use Bdiportal\Progress;
use View;
use PDF;
class CertificateController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		
		$certificates = Certificate::all();
		foreach($certificates as $certificate)
		{	
			if(Progress::expiringSoon(\Auth::id(), $certificate->id))
			{
				$progress[$certificate['id']]['expires'] = true;
			}
			elseif(Progress::expired(\Auth::id(), $certificate->id))
			{
				$progress[$certificate['id']]['expired'] = true;
			}
			$progress[$certificate['id']]['progress'] = json_decode(json_encode(Certificate::userProgress($certificate['id'])), true);
			$progress[$certificate['id']]['info'] = $certificate; 
		}

		return View::make('certificate.index')->with('certificates', $progress);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$certificate = Certificate::find($id);
		$progress = Certificate::userProgress($id);

		//Check for any progress on Certificate
		if($progress && $progress->progress != 0 && !Progress::expired(\Auth::id(), $certificate->id))
		{

			$test = Test::find($progress->next_test);
		}
		else
		{
			$test = Test::where('certificate_id', '=', $id)->first();
		}

		return View::make('certificate.begin')->with('test', $test);


	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public static function email_certificate($user, $id)
	{
		$certificate = Certificate::find($id);
		$cert_user = User::find($user);
		$progress = Certificate::getUserProgress($id, $user);
		$dealer = $cert_user->dealer;
		$corporate = $dealer->corporate;

		$certificate_view = View::make('certificate.print')->with([
			'certificate' => $certificate,
			'cert_user' => $cert_user,
			'progress' => $progress,
			'dealer' => $dealer,
			'corporate' => $corporate


			])->render();
		$outputName = str_random(10);
		$pdfPath = base_path('resources/assets/certificates').'/'.$outputName.'.pdf';
		\File::put($pdfPath, PDF::load($certificate_view, 'Letter', 'landscape')->output());

		return $pdfPath;


	}

	public function print_certificate($user, $id)
	{
		$certificate = Certificate::find($id);
		$cert_user = User::find($user);
		$progress = Certificate::getUserProgress($id, $user);
		$dealer = $cert_user->dealer;
		$corporate = $dealer->corporate;

		$certificate_view = View::make('certificate.print')->with([
			'certificate' => $certificate,
			'cert_user' => $cert_user,
			'progress' => $progress,
			'dealer' => $dealer,
			'corporate' => $corporate


			])->render();
		return PDF::load($certificate_view, 'Letter', 'landscape')->download($certificate->name);
	}


}
