<?php namespace Bdiportal\Http\Controllers;

use Input;
use Bdiportal\ShippingAddresses;
use Illuminate\Http\Request;

class ShippingController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return \View::make('shipping.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
		  'street1' => 'required',
	      'city' => 'required',
	      'state' => 'required',
	      'postal_code' => 'required',
	      'country' => 'required'
		]);

		if(Input::get('default'))
		{
			\DB::table('shipping_addresses')->where('user_id', '=', \Auth::id())->update(array('default_address' => 0));
		}

		$shipping = new ShippingAddresses;
		$shipping->street1 					= Input::get('street1');
		$shipping->city 						= Input::get('city');
		$shipping->state 						= Input::get('state');
		$shipping->postal_code 			= Input::get('postal_code');
		$shipping->country 					= Input::get('country');
		if(Input::get('default')){
			$shipping->default_address	= Input::get('default');
		}
		else{
			$shipping->default_address = 0;
		}
		$shipping->user_id					= \Auth::id();

		$shipping->save();

		\Session::flash('message', 'Successfully updated User Information!');
		return \Redirect::to('user');
	}

	public function makeDefault($id)
	{
		\DB::table('shipping_addresses')->where('user_id', '=', \Auth::id())->update(array('default_address' => 0));

		$shipping = ShippingAddresses::find($id);
		$shipping->default_address = 1;

		$shipping->save();

		return \Redirect::to('user');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		ShippingAddresses::destroy($id);
		\Session::flash('message', 'Shipping Address Removed');
		return back();
	}


}
