<?php namespace Bdiportal\Http\Controllers;
use Bdiportal\Corporate;
use Bdiportal\Dealer;
use Bdiportal\User;
use Bdiportal\Certificate;
use Bdiportal\Progress;
use Input;
use View;
use Redirect;
use Response;
use Illuminate\Http\Request;

class DealerController extends Controller {

	/**
	 * Display a listing of the resource.
	 * GET /dealer
	 *
	 * @return Response
	 */
	public function index()
	{
		$dealers = Dealer::all();
		return View::make('dealer.index')->with('dealers', $dealers);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /dealer/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$corporates = Corporate::all();
		$corporates_array = array();
		foreach($corporates as $corporate)
		{
			$corporates_array[$corporate->id] = $corporate->name;
		}
		$corporates_array[0] = 'Add new Corporate...';
		return View::make('dealer.create')->with('corporates_array', $corporates_array);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /dealer
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
		  'dealer_name' => 'required',
		  'phone' => 'required',
		  'street' => 'required',
	      'city' => 'required',
	      'state' => 'required',
	      'zip' => 'required',
	      'country' => 'required'
		]);

		

		$dealer = new Dealer;
		$dealer->name 			= Input::get('dealer_name');
		$dealer->phone 			= Input::get('phone');
		$dealer->fax 			= Input::get('fax');
		$dealer->url 			= Input::get('url');
		$dealer->street1		= Input::get('street');
		$dealer->street2		= Input::get('street2');
		$dealer->city			= Input::get('city');
		$dealer->state			= Input::get('state');
		$dealer->postal_code	= Input::get('zip');
		$dealer->country			= Input::get('country');

		if(Input::get('corporate') == 0 && Input::get('corporate_name') == True)
		{
			$this->validate($request, [
			  'corporate_name' => 'required',
			  'phone-c' => 'required',
			  'street-c' => 'required',
		      'city-c' => 'required',
		      'state-c' => 'required',
		      'zip-c' => 'required',
		      'country-c' => 'required'
			]); 
			$corporate = new Corporate;
			$corporate->name 				= Input::get('corporate_name');
			$corporate->phone 				= Input::get('phone-c');
			$corporate->fax 				= Input::get('fax-c');
			$corporate->url 				= Input::get('url-c');
			$corporate->street1 			= Input::get('street-c');
			$corporate->street2				= Input::get('street2');
			$corporate->city				= Input::get('city-c');
			$corporate->state				= Input::get('state-c');
			$corporate->postal_code			= Input::get('zip-c');
			$corporate->country				= Input::get('country-c');

			$corporate->save();

			
			$dealer->corporate_id = $corporate->id;
		} 
		else{
			$dealer->corporate_id = Input::get('corporate');

		}
		$dealer->unique_id = substr($dealer->name,0,2).''.rand(0,9999);
		


		$dealer->save();

		return Redirect::to('dealer/'.$dealer->id);
			
	}

	/**
	 * Display the specified resource.
	 * GET /dealer/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$dealer = Dealer::find($id);
		$users = $dealer->users;
		return View::make('dealer.show', ['d' => $dealer, 'users' => $users]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /dealer/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$dealer = Dealer::find($id);

		$corporates = Corporate::all();
		$corporates_array = array();
		foreach($corporates as $corporate)
		{
			$corporates_array[$corporate->id] = $corporate->name;
		}
		return View::make('dealer.edit', ['dealer' => $dealer, 'corporates_array' => $corporates_array]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /dealer/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
		  'name' => 'required',
		  'phone' => 'required',
		  'street1' => 'required',
		  'street2' => '',
	      'city' => 'required',
	      'state' => 'required',
	      'postal_code' => 'required',
	      'country' => 'required'
		]);

		$dealer = Dealer::find($id);
		$dealer->name 		= Input::get('dealer_name');
		$dealer->phone 				= Input::get('phone');
		$dealer->fax 				= Input::get('fax');
		$dealer->url 				= Input::get('url');
		$dealer->street1			= Input::get('street');
		$dealer->street2			= Input::get('street2');
		$dealer->city				= Input::get('city');
		$dealer->state				= Input::get('state');
		$dealer->postal_code		= Input::get('zip');
		$dealer->country			= Input::get('country');

		$dealer->save();

		return Redirect::to('dealer/'.$dealer->id);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /dealer/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Show Table of ALl Technicians for Specified Dealer
	 * Get /technicians/{id}
	 *
	 * @param  int  $id (Dealer id)
	 * @return Response
	 */
	public function technicians($id)
	{
		$certs = Certificate::all();

		$users = Dealer::find($id)->users;
		foreach($users as $user)
		{
			if(User::find($user->id)->hasRole('tech')) $techs[$user->id] = $user;
		}
		foreach($techs as $tech)
		{
			$tech['results'] = Certificate::allProgress($tech->id);
			
			foreach($tech['results'] as $result)
			{
				if(Progress::expired($tech->id, $result->certificate_id)){
					$result->expired = true;
				}
				else{
					$result->expired = false;
				}
			}

		}

		//return Response::json($techs);
		return View::make('dealer.technicians')->with(['techs' => $techs, 'certs' => $certs]);
	}

	public function techSearch(){

		$searchTerm = Input::get('query');
		$dealer_id = Input::get('dealer');

		$certs = Certificate::all();

		$users = User::search($searchTerm, $dealer_id)->get();
		// dd(DB::getQueryLog());
			foreach($users as $user)
			{

					if(User::find($user->id)->hasRole('tech')) $techs[$user->id] = $user;

			}
			if(isset($techs))
			{
				foreach($techs as $tech)
				{
					$tech['results'] = Certificate::allProgress($tech->id);

				}
				return View::make('dealer.techSearch')->with(['techs' => $techs, 'certs' => $certs]);
			}else
			{
				$techs = 0;
				$certs = 0;
				return View::make('dealer.techSearch')->with(['techs' => $techs, 'certs' => $certs]);
			}
	}

	public function expiringTechs()
	{
		if(Progress::getExipiringTechs())
		{
			$emails = Progress::getExipiringTechs();
			foreach($emails as $email)
			{
				$techinfo = User::find($email['user_id']);
				$today = new \DateTime();
				$today->modify('+14 day');
				$data = array(
					'user' => $techinfo->prop_name,
					'certificate' => Certificate::find($email['certificate_id']),
					'date_expires' => $today->format('m-d-Y')
				);
				//return View::make('emails.certificate.reminders')->with($data);
				\Mail::send('emails.certificate.reminders', $data, function($message) use ($techinfo) {
					$message->from('no-reply@bdindependence.com');
					$message->to($techinfo->email)
					->subject('Your B&D Certificate is About to Expire');
				});
			}
		}
		
	}
}