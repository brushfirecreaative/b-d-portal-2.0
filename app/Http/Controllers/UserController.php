<?php namespace Bdiportal\Http\Controllers;

use Bdiportal\Http\Controllers\Controller;
use Bdiportal\ShippingAddresses;
use Bdiportal\Dealer;
use Bdiportal\Corporate;
use Bdiportal\User;
use Bdiportal\Role;
use Bdiportal\Position;
use View;
use Input;
use Auth;
use Illuminate\Http\Request;

class UserController extends Controller {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	

		 $shipping = ShippingAddresses::userShipping();
		 return View::make('user.index')->with('shipping_addresses', $shipping);

	}
/**
	 * Show the form for creating a registering new tech.
	 *
	 * @return Response
	 */
	public function register($id=NUll)
	{	
		$positions = Position::all();
		$positionSelect[0] = 'Please select the level of access that best fits your position or job title.';
		
		foreach($positions as $position)
		{
			$positionSelect[$position->id] = $position->name;
		}
		$positionSelect['none'] = 'Not Listed';

		switch ($id) {

			case 'form':
				if(\Cookie::get('techRegistration'))
				{

					$dealer_id = \Cookie::get('techRegistration');
					$dealer = Dealer::find($dealer_id);
					$techs = $dealer->users;
					return View::make('sessions.techRegister')->with(['dealer' => $dealer, 'techs' => $techs, 'positions' => $positionSelect]);
				}
				else
				{
					return View::make('sessions.register')->with(['positions' => $positionSelect]);
				}
			break;
			case 'done':
				\Cookie::forget('techRegistration');
				$message = "Thank you for registering.<br> Your account is pending approval from your dealer's suppervisor. <br> You will be notified by email when your account is approved.";
				return View::make('sessions.registerBlank')->with('message', $message);
			break;
			case 'sorry':
				$message = "Sorry could not find dealer.";
				return View::make('sessions.registerBlank')->with('message', $message);
			break;

			default:
				return View::make('sessions.register');
		}
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$dealers = Dealer::all();
		$dealer_array = array();

		$allRoles = Role::all();

		foreach($allRoles as $role)
		{
			$roles[$role->id] = array(
				"id" => $role->id,
				"name" => $role->name
			); 
		}

		foreach($dealers as $dealer)
		{
			$dealer_array[$dealer->id] = $dealer->name;
		}
		return View::make('user.create', ['dealer_array'=> $dealer_array, 'roles' => $roles]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
      		'prop_name' => 'required',
      		'email' => 'required|email|unique:users',
      		'phone' => 'alpha_dash',
      		'cell-phone' => 'alpha_dash',
      		'extension' => 'alpha_dash',
      		'fax' =>'alpha_dash',
      		'password' => 'required|min:8|confirmed',
      		'position' => ''
		], [
			'prop_name.required' => 'Your full name is required',
			'email.required' => 'We need to know you email in case you forget your password',
			'email.unique' => 'The email you entered is already registered <a href="/forgot-password">Forgot Password?</a>',
			'position.required' => 'We need to know your position in order to assign you the correct permissions'
		]);
			$dealer = Dealer::find(Input::get('dealer'));

			$confirmation_code = str_random(30);

			$user = new User;
			$user->username 				= Input::get('email');
			$user->password 				=	\Hash::make(Input::get('password'));
			$user->prop_name 				= Input::get('prop_name');
			$user->email 						= Input::get('email');
			$user->phone 						= Input::get('phone');
			$user->cell_phone 						= Input::get('cell-phone');
			$user->extension 						= Input::get('extension');
			$user->fax 							= Input::get('fax');

			if(!Input::get('position_other'))
			{
				$positionQuery = Position::find(Input::get('position'));
				$user->position = $positionQuery->name;

			}
			else
			{
				$user->position = Input::get('position_other');
			}




			// dd($user->position);

			//If the User is Loged in/They are and Admin
			if(Auth::check()) {
				//dd($user);
				$user->approved_by		= Auth::id();
				$user->confirmed 				= 1;
				$user->dealer_id 				= Input::get('dealer');

				$user->save();

				$newRoles = array_values(Input::get('roles'));
				foreach($newRoles as $role)
				{
					$user->assignRole($role);
				}	

				$shipping = new ShippingAddresses;

				$shipping->street1 = $dealer->street1;
				$shipping->street2 = $dealer->street2;
				$shipping->city = $dealer->city;
				$shipping->state = $dealer->state;
				$shipping->postal_code = $dealer->postal_code;
				$shipping->country = $dealer->country;
				$shipping->default_address = 1;
				$shipping->user_id = $user->id;

				$shipping->save();

				\Session::flash('message', 'Successfully added ' .$user->username);
				return \Redirect::to('admin/users');
			}
			//If a Tech is signing up through the registration form
			else{	
				$user->approved_by				= NULL;
				$user->confirmed 					= 0;
				$user->dealer_id 					= \Cookie::get('techRegistration');
				$user->confirmation_code  = $confirmation_code;
				
				$user->save();

				$shipping = new ShippingAddresses;

				$shipping->street1 = $dealer->street1;
				$shipping->street2 = $dealer->street2;
				$shipping->city = $dealer->city;
				$shipping->state = $dealer->state;
				$shipping->postal_code = $dealer->postal_code;
				$shipping->country = $dealer->country;
				$shipping->default_address = 1;
				$shipping->user_id = $user->id;

				$shipping->save();

				$user->assignRole(1);
				

				$supervisors = Dealer::supervisors(\Cookie::get('techRegistration'));
				$data = array(
					'confirmation_code' => $confirmation_code,
					'tech' => $user
					);
				
				foreach($supervisors as $super) {
					\Mail::send('emails.registration.confirm', $data, function($message) use ($super) {
						$message->from('no-reply@bdindependence.com');
	            		$message->to($super)
	                	->subject('Pending Technician');
			        });
			       }


				return \Redirect::to('register/done');
			}
			
		}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$u = User::find($id);
		return View::make('user.show', ['u' => $u]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$u = User::find($id);
		$shipping = ShippingAddresses::userShipping($id);

		$dealers = Dealer::all();
		$allRoles = Role::all();

		foreach($allRoles as $role)
		{
			$roles[$role->id] = array(
				"id" => $role->id,
				"name" => $role->name,
				"checked" => $u->hasRole($role->name)
			); 
		}

		$dealer_array = array();
		foreach($dealers as $dealer)
		{
			$dealer_array[$dealer->id] = $dealer->name;
		}

		return View::make('user.edit', ['u' => $u, 'shipping_addresses' => $shipping, 'dealer_array' => $dealer_array, 'roles' => $roles]);
	
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$current_info = User::find($id);

		if($current_info->username == Input::get('username') || $current_info->email == Input::get('email'))
		{
			$this->validate($request, [
				'username' => 'required',
	      		'prop_name' => 'required',
	      		'email' => 'required|email',
	      		'phone' => 'alpha_dash',
	      		'cell-phone' => 'alpha_dash',
      			'extension' => 'alpha_dash',
	      		'fax' =>'alpha_dash',
	      		'password' => 'min:8|confirmed'
			]);
		}else{
			$this->validate($request, [
				'username' => 'required|unique:users',
	      		'prop_name' => 'required',
	      		'email' => 'required|email|unique:users',
	      		'phone' => 'alpha_dash',
	      		'cell-phone' => 'alpha_dash',
      			'extension' => 'alpha_dash',
	      		'fax' =>'alpha_dash',
	      		'password' => 'min:8|confirmed'
			]);
		}

			

			$user = User::find($id);
			$user->username 				= Input::get('username');
			if(Input::get('password')) $user->password =	\Hash::make(Input::get('password'));
			$user->prop_name 				= Input::get('prop_name');
			$user->email 						= Input::get('email');
			$user->phone 						= Input::get('phone');
			$user->cell_phone 						= Input::get('cell-phone');
			$user->extension 						= Input::get('extension');
			$user->fax 							= Input::get('fax');
			if(!Input::get('dealer') == null) $user->dealer_id = Input::get('dealer');
		

			$user->save();
	
			$dealer = Dealer::find($user->dealer_id);

			$dealer->street1 = Input::get('street1');
			$dealer->city = Input::get('city');
			$dealer->state = Input::get('state');
			$dealer->postal_code = Input::get('postal_code');
			$dealer->country = Input::get('country');

			$dealer->save();
			
			if(Input::get('roles'))
			{
				$newRoles = array_values(Input::get('roles'));

				$user->roles()->sync($newRoles);
			}

			\Session::flash('message', 'Successfully updated User Information!');
			return \Redirect::back();

	}

	public function dealerAll()
	{
		$positions = Position::positionSelects();
		
		$user = Auth::id();
		$userinfo = User::find($user);
		$dealers_users = Dealer::find($userinfo->dealer_id)->users;
		return View::make('user.dealer_all')->with(['users' => $dealers_users, 'positions' => $positions]);

	}

	public function corporateAll($id)
	{
		$user = Auth::id();
		$userinfo = User::find($user);
		$corporate_users = Corporate::find($id)->users;
		return View::make('user.corporate_all')->with('users', $corporate_users);
	}

	//Super Admin Only
	public function listAll()
	{
		$positions = Position::positionSelects();
		$users = User::all();

		return View::make('user.list_all')->with(['users' => $users, 'positions' => $positions]);
	}
	//Super Admin only
	public function allUnconfirmed()
	{
		$positions = Position::positionSelects();
		$users = User::where('confirmed', '=' , '0')->get();

		return View::make('user.unconfirmed')->with(['users' => $users, 'positions' => $positions]);
	}


	public function checkDealer()
	{
		$dealer = Dealer::checkID(Input::get('dealer_id'));

		if($dealer)
		{
			$cookie = \Cookie::make('techRegistration', $dealer->id, 30);
			return \Redirect::to('register/form')->with('dealer', $dealer)->withCookie($cookie);
		}
		else
		{
			return \Redirect::to('register/sorry');
		}

	}
	//Confirm Users from inside the portal
	public function confirmUser($isAjax = null)
	{
		
		$postion = Position::getRoleByName(\Input::get('positions'));
		$user = User::find(\Input::get('userId'));

		$user->confirmed = 1;
		$user->confirmation_code = null;
        $user->approved_by = Auth::id();
		$user->assignRole($postion->role_id);
		$user->save();

		$msg = 'User has been confirmed';

		\Mail::send('emails.registration.confirmed', array('username' => $user->username), function($message) use($user) {
				$message->from('no-reply@bdindependence.com');
	            $message->to($user->email)
	                ->subject('Account Approved');
	        });

		if($isAjax) return json_encode($msg);
		return \Redirect::back()->with('message', $msg);


	}

	//Confirm User from email
	public function confirmRegistration($confirmation_code)
	{
		if( ! $confirmation_code)
        {
            return View::make('sessions.registerBlank')->with('message', 'We encountered and error confirming technician.');
        }
        //Get User based on confirmation code
        $user = User::whereConfirmationCode($confirmation_code)->first();

        //No user found with confirmation code
        if ( ! $user)
        {
            return View::make('sessions.registerBlank')->with('message', 'We encountered and error confirming technician.');
        }

        //Get Positions
      	$positions = Position::all();
		$positionSelect[0] = 'Please select the level of access for this user.';
		
		foreach($positions as $position)
		{
			$role = Position::find($position->id)->role;
			$positionSelect[$position->id] = ''.$position->name.' ('.$role->display_name.')';
		}


	//dd($user);
      return View::make('sessions.registerRoles')->with(['confirming_user' => $user, 'roles' => $positionSelect]);
    }

    public function storeRegistration()
    {
    	$user = User::find(\Input::get('user'));
    	$user->confirmed = 1;
        $user->confirmation_code = null;
        $user->approved_by = Auth::id();
        $user->save();

        $role = Position::find(\Input::get('role'))->role;


        $user->assignRole($role->id);


        \Session::flash('message','You have successfully confirmed '.$user->prop_name.'\'s account.');

        \Mail::send('emails.registration.confirmed', array('username' => $user->username), function($message) use($user) {
				$message->from('no-reply@bdindependence.com');
	            $message->to($user->email)
	                ->subject('Account Approved');
	        });


        return \Redirect::to('/users');
    }

    public function dealerSearch() {

    	$searchTerm = Input::get('query');
		$dealer_id = Input::get('dealer');
    
		$dealers_users = User::search($searchTerm, $dealer_id)->get();
		//dd($dealers_users);
		return View::make('user.userSearch')->with('users', $dealers_users);

    }

     public function corporateSearch() {

    	$searchTerm = Input::get('query');
		$corporate_id = Input::get('corporate');
    
		$corporate_users = Corporate::find($corporate_id)->users;
		$users = array();
		foreach ($corporate_users as $user)
		{

			if(strpos($user->prop_name, $searchTerm) !== false){
				
				$users[] = $user;
				
			}
			else if(strpos($user->email, $searchTerm) !== false){
				
				$users[] = $user;
			}
		}
		
		//dd($dealers_users);
		return View::make('user.corporateSearch')->with('users', $users);

    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::destroy($id);

		$msg = 'User and all associated data has been removed.';
		return \Redirect::to('users')->with('message', $msg);
	}


}
