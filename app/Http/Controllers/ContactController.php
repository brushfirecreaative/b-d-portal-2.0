<?php

namespace Bdiportal\Http\Controllers;

use Illuminate\Http\Request;

use Bdiportal\Http\Requests;
use Bdiportal\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['name'] = \Input::get('name');
        $data['email'] = \Input::get('email');
        $data['phone'] = \Input::get('phone');
        $data['dealer'] = \Input::get('dealer');
        $data['dealer_info'] = \Input::get('dealer_info');

        \Mail::send('emails.contact', $data, function($message){
            $message->from('no-reply@bdindependence.com');
            $message->to('info@bdindependence.com')
            ->subject('Trouble Logging in to the Portal');
        });
        $msg = 'Thank you for your submission! Someone from B&amp;D Independence will be contacting you soon to verify your credentials and locate your Dealer ID.';
        return \Redirect::back()->with('message', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
