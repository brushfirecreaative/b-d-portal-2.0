<?php namespace Bdiportal\Http\Controllers;
use Bdiportal\Category;
use Bdiportal\Document;
use View;
use Input;

class KnowledgebaseController extends Controller {

	/**
	 * Display a listing of the resource.
	 * GET /knowledgebase
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = \Auth::user();

		$categories = Category::all();

		foreach($categories as $category)
		{
			if($category->parent == 0)
			{
				if($category->id == 4) //Pricing and Sales Tools
				{
					if($user->hasRoleOrHigher(3)) //Higher Than a Technician
					{
						$cats[] = array('name' => $category['name'], 'children' => $category->children);
					}
				}
				else
				{
					$cats[] = array('name' => $category['name'], 'children' => $category->children);
				}
				
			}
		}
		return View::make('knowledgebase.index', compact('cats'));
	}
/**
 * Document Search
 * @return [type] [description]
 */
	public function docSearch()
	{	
		$user = \Auth::user();
		$q = Input::get('query');

		$docs = Document::whereRaw(
			"MATCH(title,body) AGAINST(? IN BOOLEAN MODE)", 
			array($q)
		)->get();
		
		foreach($docs as $key => $doc)
				{
					$tier1 = Category::find($doc->category_id)->parent;
					if($tier1 == 4 && !$user->hasRoleOrHigher(3))
					{
						unset($docs[$key]);
					}
					else
					{
						$doc->preview = Document::getPreview($doc);
					}
				}
		return View::make('knowledgebase.search', compact('docs'));

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /knowledgebase/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /knowledgebase
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /knowledgebase/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function category($id)
	{

			$category = Category::find($id);
			$children = $category->children;
			$documents = array();
			if($children->count() != 0)
			{
				foreach($children as $child)
				{
					$child_cat = Category::find($child->id);
					$documents[$child->id]['name']= $child_cat->name;
					$documents[$child->id]['docs'] = $child_cat->documents;

					foreach($documents[$child->id]['docs'] as $doc)
					{
						$doc->preview = Document::getPreview($doc);
					}
				}
			}
			else
			{
				$documents = $category->documents;
				foreach($documents as $doc)
				{
					$doc->preview = Document::getPreview($doc);
				}
			}
			//return Response::json($children);
			return View::make('knowledgebase.category')->with(compact('documents', 'category'));
	}

	/**
	 * Display the specified resource.
	 * GET /knowledgebase/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /knowledgebase/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /knowledgebase/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /knowledgebase/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}