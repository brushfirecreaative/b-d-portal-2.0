<?php namespace Bdiportal\Http\Controllers;
use Bdiportal\Forms\MinivanOrderForm;

use View;
use Auth;
use Input;
use Session;
use Response;
use DB;
use Bdiportal\Forms;
use Bdiportal\ShippingAddresses;
use Bdiportal\Conversion;
use Bdiportal\ConversionCompany;
use Bdiportal\Series;
use Bdiportal\VYears;
use Bdiportal\Product;
use Bdiportal\MakeModel;
use Bdiportal\Accessory;
use Bdiportal\Option;
use Bdiportal\User;
use Bdiportal\Order;
use Bdiportal\Brackets;
use Bdiportal\Applications;
use Bdiportal\Station;
use Bdiportal\HtMedia;
use Log;

class OrderController extends Controller {

	/**
		 * 
		 * @var \Bdiportal\Forms\MinivanOrderForm
		 */
		protected $minivanOrderForm;

		/**
		 * 
		 * @param User $userForm 
		 */
		function __construct(MinivanOrderForm $minivanOrderForm)
		{
			$this->minivanOrderForm = $minivanOrderForm;
		}

	/**
	 * Display a listing of the resource.
	 * GET /order
	 *
	 * @return Response
	 */
	public function index()
	{
		$forms = Forms::all();
		return View::make('order.index')->with('forms', $forms);	
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /order/create
	 *
	 * @return Response
	 */
	public function create($id)
	{
		$form = Forms::find($id);
		foreach(ShippingAddresses::userShipping() as $address)
		{
			$shipping[$address['id']] = $address['street1'] .' ' .$address['city'] . ', ' .$address['state'] . ' ' . $address['postal_code'];
		}
		$default_shipping = ShippingAddresses::defaultAddress(); 

		//If Special Vans get make models
		if($form->view == 'special' || $form->view == 'fullsize') {
				$options['makeModel'][0] = 'Make/Model';
			foreach(MakeModel::where('form_id', $form->id)->get() as $make)
			{
				$options['makeModel'][$make->id] = $make->name; 
			}
		}
		elseif($form->view == 'ht') {
			//Get all from brackets table
			$all_brackets = Brackets::all();
			$wc_makes[0] = 'Choose Wheelchair Make...';
			//Make array of only Makes 
			foreach($all_brackets as $bracket){
				$wc_makes[$bracket->manufacturer] = $bracket->manufacturer;  		
			}

			$wc_makes = array_unique($wc_makes);

			$options['wc_make'] = $wc_makes;

			$ht_actions = Applications::where('parent_id', 3)->get();
			$action_selects[0] = '';
			foreach($ht_actions as $actions)
			{
				$action_selects[$actions->id] = $actions->name; 
			}
			$options['ht_action'] = $action_selects;

			$ht_locate = Applications::where('parent_id', 4)->get();
			$locate_selects[0] = '';
			foreach($ht_locate as $located)
			{
				$locate_selects[$located->id] = $located->name; 
			}
			$options['ht_located'] = $locate_selects;

			$options['conversion'] = array(
				'' => '',
				'Braun' => 'Braun',
				'Eldorado' => 'Eldorado',
				'VMI' => 'VMI',
				'Other' => 'Other',
				'Non-Lowered Floor' => 'Non-Lowered Floor'
			);
		}
		//End Hightower Specifics

		//All Dropdown Options
		$v_years_range = range(2001,date("Y"));
		$v_years = array_combine($v_years_range, $v_years_range);
		$v_years_select = array('' => 'Choose Year') + $v_years;
		$options['v_years'] = $v_years_select;
		$options['ramp'] = array('' => 'Choose Ramp Type', 'floor' => 'In-Floor Ramp', 'folding' => 'Folding Ramp', 'none' => 'No Ramp');

		return View::make('order.create')->with([
			'form' => $form, 
			'shipping' => $shipping,
			'default_shipping' => $default_shipping,
			'options'	=> $options
			]);
	}

	/**
	 * Review the Order before placing.
	 * POST /order
	 *
	 * @return Response
	 */
	public function reviewOrder() 
	{	
		$form_data = Input::all();
		$formView = $form_data['form'];
		
		//Get Information for the selected Vehicle 
		$order['shipping'] = ShippingAddresses::find($form_data['shipping_address'])->toArray();
		$order['billing'] = ShippingAddresses::find($form_data['billing_address'])->toArray();
		
		if($formView == 'minivan' || $formView == 'fullsize'){
			$order['makeModel'] = MakeModel::find($form_data['vehicle_make'])->toArray();
			$order['conversionBy'] = ConversionCompany::find($form_data['conversion_by'])->toArray();
			$order['conversionType'] = Conversion::find($form_data['conversion_type'])->toArray();


			//Get information on the selected seatbase with Accesories and options
			foreach($form_data['seatbase'] as $seatbase)
			{
				//Get each seatbase info
				$order['seatbases'][$seatbase]['info'] = Product::find($seatbase)->toArray();
				foreach($form_data['options'][$seatbase] as $acc_key => $acc_value)
				{
					//Get each Accessory info
					$order['seatbases'][$seatbase]['accessories'][$acc_key]['info'] = Accessory::find($acc_key)->toArray();
					foreach($form_data['options'][$seatbase][$acc_key] as $option)
					{
						//Get each options info
						$order['seatbases'][$seatbase]['accessories'][$acc_key]['options'][$option] = Option::find($option)->toArray();
					}
				}
			}
		}
		elseif($formView == 'specail'){
			$order['makeModel'] = MakeModel::find($form_data['vehicle_make'])->toArray();
		}
		elseif($formView == 'ht'){
			$order['bracket'] = Session::get('bracket');
			$order['station'] = Session::get('station');
			if(Session::get('floor_mount')){
				$order['floor_mount'] = Session::get('floor_mount');
			}
			

			$application = Applications::find($form_data['application']);
			$order['application'] = $application->name;

			$action = Applications::find($form_data['ht_action']);
			$order['action'] = $action->name;

			$located = Applications::find($form_data['ht_located']);
			$order['located'] = $located->name;
			if($form_data['ht_vehicle'] != 0) $order['makeModel'] = MakeModel::find($form_data['ht_vehicle'])->toArray();
			$order['conversionBy'] = $form_data['ht_conversion'];
		}


		
		

		// print_r($form);
		// print_r($order);
		Session::put('form_data', $form_data);
		Session::put('order', $order);
		return View::make('order.review-'.$formView)->with([
			'form_data' => $form_data,
			'order' => $order
		]);
		
		
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /order
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = User::find(Auth::id());

		$form_data = Session::get('form_data');

		$form = Forms::where('view', $form_data['form'])->first();
		

		$order_data = Session::get('order');
		if($order_data){
			if($form_data['form'] == 'minivan') {
				$order_data['vehicle_year'] = $form_data['vehicle_year'];
				$order_data['conversion_year'] = $form_data['conversion_year'];
				$order_data['ramp'] = $form_data['ramp'];
			}
			elseif($form_data['form'] == 'ht'){
				$order_data['wheelchair_make'] = $form_data['wheelchair_make'];
				$order_data['wheelchair_model'] = $form_data['wheelchair_model'];
			}
			$order = new Order;
			$order->dealer_id = $user->dealer->id;
			$order->ordered_by = $user->id;
			$order->po = $form_data['po'];
			$order->order = serialize($order_data);

			$order->save();

			Session::forget('form_data');
			Session::forget('order');
			$form_data['form_name'] = $form->name;
			$data = array(
				'form_data' => $form_data, 
				'order' => $order_data
			);
			//return View::make('emails.orders.confirmation')->with(['form_data' => $form_data, 'order' => $order_data]);
			\Mail::send('emails.orders.confirmation', $data, function($message) use($form_data)
			{
				$message->from('no-reply@bdindependence.com', 'B&amp;D Independence');
	            $message->to($form_data['email'], $form_data['ordered_by'])->subject('New Order');
	        });

	        \Mail::send('emails.orders.confirmation', $data, function($message) use($form_data)
			{
				$message->from('no-reply@bdindependence.com', 'B&amp;D Independence');
	            $message->to('zlewis@bdindependence.com', 'Zach Lewis')->subject('New Order');
	        });

	  
	        return View::make('order.confirmation');
		}
		else{
			return \Redirect::to('order');
		}
		
	}

	/**
	 * Display the specified resource.
	 * GET /order/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /order/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /order/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /order/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function conversions()
	{
		$id = $_POST['cb'];

		$conversions = Conversion::where('company_id', '=', $id)->get();

		return Response::json($conversions);
	}

	public function makeModel(){
		$v_year = $_POST['v_year'];

		//Get all makes and models with min and max dates that fit
		$makeModel_Max = \DB::table('v_years')->where('min_year', '<=', $v_year)->where('max_year', '>=', $v_year)->get();

		//Get all makes and models with min and and no max dates that fit
		$makeModel_No_Max = \DB::table('v_years')->where('min_year', '<=', $v_year)->where('max_year', '=', NULL)->get();

		//Combine both arrays
		$makeModel = array_merge($makeModel_Max, $makeModel_No_Max);

		//establish $mm_ids array
		$mm_ids = array();

		// Form an array for make_model_ids
		foreach($makeModel as $mm){
			array_push($mm_ids, $mm->make_model_id);
		}

		// Sort array for only unique values
		$mm_ids_unique = array_values(array_unique($mm_ids));

		// Form an associative array of mm_ids and names
		foreach ($mm_ids_unique as $mm_unique) {
			$mm_full = MakeModel::find($mm_unique);
			$mm_results[$mm_unique] = $mm_full->name;
		}


		return Response::json($mm_results);
	}

	public function conversionBy(){
		$mm = $_POST['mm'];
		$form_id = $_POST['form_id'];
		if($form_id == 1)
		{

			$v_year = $_POST['v_year'];

			$array_Max = DB::table('v_years')->where('min_year', '<=', $v_year)->where('max_year', '>=', $v_year)->where('make_model_id', '=', $mm)->get();

			//Get all makes and models with min and and no max dates that fit
			$array_No_Max = DB::table('v_years')->where('min_year', '<=', $v_year)->where('max_year', '=', NULL)->where('make_model_id', '=', $mm)->get();

			//Combine both arrays
			$conversions = array_merge($array_Max, $array_No_Max);


			//establish $conv_ids array
			$conv_ids = array();

			// Form an array for conversion_companies_ids
			foreach($conversions as $conv){
				array_push($conv_ids, $conv->conversion_companies_id);
			}

			//Sort array for only unique values
			$conv_ids_unique = array_values(array_unique($conv_ids));

			// Form an associative array of conv_ids and names
			foreach ($conv_ids_unique as $conv_unique) {
				$conv_full = ConversionCompany::find($conv_unique);
				$conv_results[$conv_unique] = $conv_full->name;
			}
		}
		else if($form_id == 2)
		{
			foreach(ConversionCompany::where('form_id', $form_id)->get() as $cc)
			{
				$conv_results[$cc->id] = $cc->name;
			}
		}
		return Response::json($conv_results);


	}	

	public function minivans()
	{
		$ct = $_POST['ct'];
		$conversion = Conversion::find($ct);

		$series = Series::find($conversion->series_id);

		$products = $series->products;

		foreach ($products as $product)
		{
			$options['seatbases'][$product->id] = $product->name;
		}

		return View::make('order.minivan')->with([
			'series' => $series,
			'products' => $products,
			'options' =>  $options
			]);

		
	}

	public function accessories()
	{
		$id = $_POST['id'];

		$accessories = Product::find($id)->accessories;
		foreach($accessories as $accessory)
		{
			$accessory['options']= Accessory::find($accessory->id)->options;

		}
		return View::make('order.accessories')->with(['accessories' => $accessories, 'product' => $id]);
		//return Response::json($accessories);
	}

	public function wc_models()
	{
		$make = $_POST['make'];
		$make_brackets = Brackets::where('manufacturer', $make)->get()->toArray();

		//Make array of only Makes 
		foreach($make_brackets as $bracket){
			$models[$bracket['series']] = $bracket['series'];  		
		}
		$models = array_unique($models);

		$apps = Applications::where('parent_id', 0)->get();
		foreach ($apps as $ap) {
			$aps[$ap->id] = $ap->name;
		}


		$results = array('models' => $models, 'ap' => $aps);

		return Response::json($results);

	}

	public function get_ht()
	{

		$ap = $_POST['ap'];
		$wc_make = $_POST['make'];
		$wc_model = $_POST['model'];
		$vehicle = $_POST['vehicle'];
		$side = $_POST['side'];
		$conversion = $_POST['conversion'];
		$ht_action = $_POST['ht_action'];

		$bracket = Brackets::get_bracket($wc_make, $wc_model, $ap);

		$station = Station::ht_station($ap, $vehicle, $side);
		if(!$ht_action) $ht_action = null;
		$graphic = HtMedia::media($ap, $ht_action, $side);

		//Get Floor Mount Kit

		if($conversion == 'Braun'){
			$floor_mount = ' HTB089';
		}
		elseif($conversion == 'VMI' && $vehicle == '13'){
			$floor_mount = ' HTB089';
		}
		elseif($conversion == 'Eldorado'){
			$floor_mount = ' HTB090';
		}
		elseif($conversion == 'Non-Lowered Floor'){
			$floor_mount = ' None';
		}
		else{
			$floor_mount = '';
		}

		//Get Graphic if needed
		



		// if(isset($_POST['vehicle'])){
		// 	$vehicle = $_POST['vehicle'];
		// 	$station = Station::ht_station($ap, $vehicle, $side);
		// }
		// elseif(isset($_POST['side'])){

		// }
		// else
		// {
		// 	$station = Station::ht_station($ap, null, $side);
		// }
		

		$results = array(
			'bracket' => $bracket,
			'station' => $station,
			'floor_mount' => $floor_mount,
			'graphic' => $graphic
		);

		Session::forget('bracket', $bracket);
		Session::forget('station', $station);
		Session::forget('floor_mount', $floor_mount);

		Session::put('bracket', $bracket);
		Session::put('station', $station);
		Session::put('floor_mount', $floor_mount);

		Log::debug($results);
		return Response::json($results);
	}


}