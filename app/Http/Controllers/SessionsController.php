<?php namespace Bdiportal\Http\Controllers;
use Illuminate\Support\MessageBag;
use View;
use Auth;
use Redirect;
use Session;
use Bdiportal\User;
use Bdiportal\Dealer;
use Bdiportal\LegacyUser;

class SessionsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($email = null)
	{
		$iam = array(
			'' => 'I have...',
			'tech' => 'I have a Technician login from the previous portal',
			'dealer' => 'I have only a Dealer login',
			'id' => 'I have only a Dealer ID',
			'nothing' => 'I have nothing'
		);
		if($email)
		{
			return View::make('sessions.create')->with(['iam' => $iam, 'email' => $email]);
		}
		if(Auth::check()) return Redirect::to('/portal');
		return View::make('sessions.create')->with('iam', $iam);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		Session::forget('message');
		//Check to see if User is a Legacy User. Remove after 3-6 Months
		if(User::isLegacy(\Input::get('username'))){
			//If they are a Legacy check credentials 
			if(LegacyUser::login(\Input::get('username'), \Input::get('password'))) {
				$dealer_id = User::legacyUserId(\Input::get('username'), \Input::get('password'));
				$cookie = \Cookie::make('techRegistration', $dealer_id, 30);

				$dealer = Dealer::find($dealer_id);

				return \Redirect::to('register/form')->with('dealer', $dealer)->withCookie($cookie);
			}
			else{
				$errors = new MessageBag(['password' => ['Username and/or Password were incorrect']]);
				return Redirect::back()->withInput()->withErrors($errors);
			}
		}
		$credentials = [
			'username' => \Input::get('username'),
			'password' => \Input::get('password'),
			'confirmed' => 1
		];


		//Start Portal 2.0 Sign in
		if(User::exsists(\Input::get('username')))
		{
			if(User::checkConfirmed(\Input::get('username')))
			{
					
				if(Auth::attempt($credentials))
				{
					Session::forget('message');
					Session::forget('credentials', $credentials);
					return Redirect::intended('/portal');
				}
				else{
					$errors = new MessageBag(['password' => ['Username and/or Password were incorrect']]);

					Session::put('message', 'Login Failed');
					Session::put('credentials', $credentials);
					return Redirect::back()->withInput()->withErrors($errors);
				}
			}
			else
			{
				Session::put('message', 'Not Confirmed');
				Session::put('credentials', $credentials);
				$errors = new MessageBag(['confirmed' => ['Your account has not yet been approved. Please contact your supervisor.']]);
				return Redirect::back()->withInput()->withErrors($errors);
			}
		}
		else
		{
			Session::put('message', 'Login Failed');
			Session::put('credentials', $credentials);
			$errors = new MessageBag(['confirmed' => ["The Username does not exsist"]]);
			return Redirect::back()->withInput()->withErrors($errors);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();

		return Redirect::to('/login');
	}


}
