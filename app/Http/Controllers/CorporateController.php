<?php namespace Bdiportal\Http\Controllers;
use Bdiportal\Certificate;
use Bdiportal\Corporate;
use Bdiportal\User;

class CorporateController extends Controller {

	/**
	 * Display a listing of the resource.
	 * GET /corporate
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /corporate/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /corporate
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /corporate/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /corporate/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /corporate/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /corporate/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Show Table of ALl Technicians for Specified Dealer
	 * Get /technicians/{id}
	 *
	 * @param  int  $id (Dealer id)
	 * @return Response
	 */
	public function technicians($id)
	{
		$certs = Certificate::all();
		$corporation = Corporate::find($id);
		$users = $corporation->users;
		foreach($users as $user)
		{
			if(User::find($user->id)->hasRole('tech')) $techs[$user->id] = $user;
		}
		foreach($techs as $tech)
		{
			$tech['results'] = Certificate::allProgress($tech->id);

		}
		//return Response::json($techs);
		return \View::make('corporate.technicians')->with(['techs' => $techs, 'certs' => $certs, 'corporation' => $corporation]);
	}

}