<?php

namespace Bdiportal\Http\Controllers;

use Illuminate\Http\Request;

use Bdiportal\Http\Requests;
use Bdiportal\Http\Controllers\Controller;
use Bdiportal\Warranty;
use Bdiportal\User;
use Bdiportal\ProductModel;

class WarrantyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warranties = Warranty::all();

        foreach($warranties as $warranty)
        {
            if($warranty->model_number == '- Transfer Seat Bases -' || $warranty->model_number == NULL)
            {
                $warranty->model_number = null;
            }
            elseif(strlen($warranty->model_number) < 4)
            {
                 $warranty->model_number = ProductModel::find($warranty->model_number)->model;
                // if(ProductModel::find($warranty->model_number) == null)
                //     {
                //         echo $warranty->id . ' - ' . $warranty->model_number . '<br>';
                //     }
          
            }
            $warranty->notes = $warranty->notes->count();
            
        };
        //dd('done');
        return view('warranty.search')->with(['warranties' => $warranties]);
    }

     public function search(Request $request)
    {
        $query = $request->input('query');
        $warranties = Warranty::where('serial_number', '=', $query)->get();

        foreach($warranties as $warranty)
        {
            if($warranty->model_number == '- Transfer Seat Bases -' || $warranty->model_number == NULL)
            {
                $warranty->model_number = null;
            }
            elseif(strlen($warranty->model_number) < 4)
            {
                $warranty->model_number = ProductModel::find($warranty->model_number)->model;
            }

            //dd($w->info);
        };

        return view('warranty.search')->with(['warranties' => $warranties]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $API_KEY = 'SJ1zwQMP^yA@HsyM017TR!tYxjB2wMah';
        if($request->input('API_KEY') == $API_KEY){
            $card = new Warranty();
            $card->serial_number = $request->input('serial_number');
            $card->date = $request->input('date');
            $card->product = $request->input('product');
            $card->dealer = $request->input('dealer_name');
            $card->dealer_city = $request->input('dealer_city');
            $card->dealer_state = $request->input('dealer_state');
            $card->dealer_zip = $request->input('dealer_zip');
            $card->model_number = $request->input('model_number');
            $card->serial_number = $request->input('serial_number');
            $card->first_name = $request->input('first_name');
            $card->last_name = $request->input('last_name');
            $card->street = $request->input('street');
            $card->city = $request->input('city');
            $card->state = $request->input('state');
            $card->zip = $request->input('zip');
            $card->phone = $request->input('phone');
            $card->email = $request->input('email');
            $card->save();


            return $card;
        }

        return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $warranty = Warranty::find($id);

        if($warranty->model_number == '- Transfer Seat Bases -' || $warranty->model_number == NULL)
        {
            $warranty->model_number = null;
        }
        elseif(strlen($warranty->model_number) < 4)
        {
            $warranty->model_number = ProductModel::find($warranty->model_number)->model;
        }

        $warranty->notes;
        foreach($warranty->notes as $note)
        {
            $note->user = User::find($note->user_id);
            $note->date = date('m/d/Y', strtotime($note->created_at));
        }
        //dd($warranty->notes);
        return view('warranty.single')->with(['warranty' => $warranty]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->input('model_number') == null)
        {
            $model_number = null;
        }
        else
        {
            $model = ProductModel::where('model' , '=', $request->input('model_number'))->first();
            $model_number = $model->id;
        }
        
        
        $card = Warranty::find($id);
            $card->serial_number = $request->input('serial_number');
            $card->date = $request->input('date');
            $card->product = $request->input('product');
            $card->dealer = $request->input('dealer');
            $card->dealer_city = $request->input('dealer_city');
            $card->dealer_state = $request->input('dealer_state');
            $card->dealer_zip = $request->input('dealer_zip');
            $card->model_number = $model_number;
            $card->serial_number = $request->input('serial_number');
            $card->first_name = $request->input('first_name');
            $card->last_name = $request->input('last_name');
            $card->street = $request->input('street');
            $card->city = $request->input('city');
            $card->state = $request->input('state');
            $card->zip = $request->input('zip');
            $card->phone = $request->input('phone');
            $card->email = $request->input('email');
            $card->save();

        return redirect('admin/warranty/'.$id)->with('status', 'Warranty Card Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
