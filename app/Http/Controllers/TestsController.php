<?php namespace Bdiportal\Http\Controllers;

use Bdiportal\Test;
use Bdiportal\Certificate;
use Bdiportal\Result;
use Bdiportal\Progress;
use Bdiportal\User;
use View;
use Input;
use Auth;
use Session;
use Redirect;
use Cookie;
use Response;


class TestsController extends Controller {


	/**
	 * Display the specified resource.
	 * GET /tests/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function training($id)
	{
		$test = Test::find($id);

		\Cookie::queue('takingTest', 1, 90);
		return View::Make('tests.training')->with('test', $test);
	}

	/**
	 * Display the specified resource.
	 * GET /tests/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
			$test = Test::find($id);
			foreach($test->questions as $question)
			{
				$questions[] = array('content' => $question['content'], 'anwsers' => $question->anwsers);
			}
		
			return View::Make('tests.show')->with(['test'=> $test, 'questions' => $questions]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tests/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		//Get Test Taken
		$test = Test::find($id);
		
		//Check Anwsers against Key
		foreach($test->questions as $question)
		{
			if (Input::get($question['id']) == $question['anwser_id'])
	        {
	            $graded[$question['id']] = '1'; //Correct
	        }
	        else
	        {
	            $graded[$question['id']] = '0'; //Wrong
	        }
		}

		//Calculate Percentage
		$numberofquestions = count(Input::all());
		$answered = array_count_values($graded);
	    $correct = 0;
	    if (isset($answered['1'])) {$correct = $answered['1'];}

	    if($correct != 0)
	    {
	    	$score = round(($correct / ($numberofquestions - 2)) * 100);
	    }
	    else{
	    	$score = 0;
	    }

	    //Save Results
	    $result = Result::firstOrNew(['user_id' => Auth::id(), 'test_id' => $id]);
	    $result->user_id = Auth::id();
	    $result->test_id = $id;
	    $result->score = $score;
	    $result->attempts = $result->attempts + 1;

	    $result->save();


	    


	    //Update Progress
	    $progress = Progress::firstOrNew(['certificate_id' => $test['certificate_id'], 'user_id' => Auth::id()]);
	    $progress->certificate_id = $test['certificate_id'];
	    $progress->user_id = Auth::id();

	    //Get info for certificate
	    $certificate = Certificate::find($test['certificate_id']);
		foreach($certificate->tests as $t)
		{
			$tests[] = $t['id'];
		}
	    
	    $i = array_search($id, $tests);
	    $next_test_i = $i + 1;
	    $numberoftests = count($tests);
	    $passfail = 'fail';
	    //If Tests passes
		if($score >= 80)
	    {
	    	if(array_key_exists($next_test_i, $tests))
	    	{
	    		$progress->next_test = $tests[$next_test_i];
	    	}
	    	else
	    	{
	    		$progress->next_test = NULL;
	    	}
	    	$progress->progress = (($i + 1) / $numberoftests) * 100;
	    	$passfail = 'pass';
	    	// You this is last test of Certificate
	    	if($progress->progress == 100)
	    	{
	    		$progress->completed_on = date('Y-m-d H:i:s');

	    		//Prepare and send email to B&D
	    		$user = Auth::user();
			    $emailData = array(
			    	'user' => $user->prop_name,
			    	'cert' => $certificate
			    );
			    $data = array(
			    	'user' => Auth::user(),
			    	'attachment' => CertificateController::email_certificate($user->id, $certificate->id)
			    );

			    \Mail::send('emails.certificate.certificate', $emailData, function ($m) use ($data) {
		            $m->from('eric@brushfire-design.com', 'B&ampD Dealer Portal');

		            $m->to($data['user']->email, $data['user']->name)->cc('zlewis@bdindepence.com')->subject('Your Certificate');

		            $m->attach($data['attachment']);
		        });

		        \File::delete($data['attachment']);
			  }
	    
	    }

	    $progress->save();

	    Session::flash('score', $score);
	    Session::flash('passfail', $passfail);
	    Session::flash('certificate', $test['certificate_id']);
	    Session::flash('next_test', $progress->next_test);




	   	return Redirect::to('/tests/completed');

	}

	public function completed()
	{
			$passfail = Session::get('passfail');
			$score = Session::get('score');
			$certificate = Session::get('certificate');
			$next_test = Session::get('next_test');
			
			$cookie = Cookie::forget('takingTest');

			$view = View::make('tests.complete')->with([
				'passfail' => $passfail, 
				'score' => $score, 
				'certificate' => $certificate,
				'next_test' => $next_test
				]);

			return Response::make($view)->withCookie($cookie);
	}

}