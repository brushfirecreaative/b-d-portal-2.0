<?php namespace Bdiportal\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		 \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Bdiportal\Http\Middleware\VerifyCsrfToken::class,
        \GeneaLabs\LaravelCaffeine\Http\Middleware\LaravelCaffeineDripMiddleware::class,

	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => \Bdiportal\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \Bdiportal\Http\Middleware\RedirectIfAuthenticated::class,
		'role' => \Zizaco\Entrust\Middleware\EntrustRole::class,
    	'permission' => \Zizaco\Entrust\Middleware\EntrustPermission::class,
    	'ability' => \Zizaco\Entrust\Middleware\EntrustAbility::class,
    	'takingTest' => \Bdiportal\Http\Middleware\TakingTest::class,
    	 
	];

}
