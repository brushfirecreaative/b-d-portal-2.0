<?php namespace Bdiportal;

class MakeModel extends \Eloquent {
	protected $table = 'make_model';

	protected $fillable = [];

  public function vYears()
  {
    return $this->hasMany('Bdiportal\VYears');
  }

  public function conversions()
  {
  	return $this->hasManyThrough('Bdiportal\Conversion', 'Bdiportal\VYears');
  }
}