<?php

namespace Bdiportal;

use Illuminate\Database\Eloquent\Model;

class LegacyProgress extends Model
{
    protected $table = 'legacy_progress';
}
