<?php namespace Bdiportal;

use Bdiportal\ConversionCompany;

class ConversionCompany extends \Eloquent {
	protected $fillable = ['name'];

  protected $table = 'conversion_companies';

  public static function selects()
  {
    foreach(ConversionCompany::all() as $option)
    {
      $selects[$option->id] = $option->name;
    }

    return $selects;
  }
}