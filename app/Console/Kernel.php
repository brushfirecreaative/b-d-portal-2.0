<?php namespace Bdiportal\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Bdiportal\Progress;
use Bdiportal\Certificate;
use Bdiportal\User;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'Bdiportal\Console\Commands\Inspire',
		'Bdiportal\Console\Commands\AssignIds',
		'Bdiportal\Console\Commands\migrateTechs'
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->call(function () {
			if(Progress::getExipiringTechs())
			{
				$emails = Progress::getExipiringTechs();
				foreach($emails as $email)
				{
					$techinfo = User::find($email['user_id']);
					$today = new \DateTime();
					$today->modify('+14 day');
					$data = array(
						'user' => $techinfo->prop_name,
						'certificate' => Certificate::find($email['certificate_id']),
						'date_expires' => $today->format('m-d-Y')
					);
					//return View::make('emails.certificate.reminders')->with($data);
					\Mail::send('emails.certificate.reminders', $data, function($message) use ($techinfo) {
						$message->from('no-reply@bdindependence.com');
						$message->to($techinfo->email)
						->subject('Your B&D Certificate is About to Expire');
					});
				}
			}
		})->daily()->at('6:00');
	}

}
