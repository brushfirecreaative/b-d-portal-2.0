<?php

namespace Bdiportal\Console\Commands;

use Illuminate\Console\Command;
use Bdiportal\LegacyTechs;
use Bdiportal\LegacyProgress;
use Bdiportal\LegacyResult;
use Bdiportal\User;
use Bdiportal\Progress;
use Bdiportal\Result;

class migrateTechs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrateLegacy:techs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        //Get all legacy Users
        $techs = LegacyTechs::all();
        $bar = $this->output->createProgressBar(count($techs));

        foreach($techs as $tech)
        {
            $bar->advance();
            //Insert a new user for each legacy tech
            $user = new User;

            $user->username = $tech->email;
            $user->password = $tech->password;
            $user->email = $tech->email;
            $user->confirmed = 1;
            $user->prop_name = $tech->first_name .' ' .$tech->last_name;
            $user->dealer_id = $tech->dealer_id;
            $user->approved_by = 5;

            $user->save();

            //Assign Tech Role
            $user->assignRole(2);

            //Get all progress for Legacy Tech
            $legacy_progress = LegacyProgress::where('tech_id', '=', $tech->id)->get();
            foreach($legacy_progress as $lp){
                $progress = new Progress;
                 
                 $progress->certificate_id = $lp->cert_id;
                 $progress->user_id = $user->id;
                 if($lp->next_test == 0)
                 {
                    $progress->next_test = NULL;
                 }
                 else{

                    $progress->next_test = $lp->next_test;
                }
                 $progress->progress = $lp->progress;
                 $progress->completed_on = $lp->date_completed;

                 $progress->save();
            }

            //Get all results for Legacy Tech
            $legacy_results = LegacyResult::where('tech_id', '=', $tech->id)->get();
            foreach($legacy_results as $lr)
            {
                $result = new Result;

                $result->user_id = $user->id;
                $result->score = $lr->score;
                $result->attempts = $lr->attempts;


                //We need to switch Test 1 & 2 
                if($lr->test_id == 1)
                {
                    $result->test_id =  2;
                }
                elseif($lr->test_id == 2)
                {
                    $result->test_id = 1;
                }
                else{
                    $result->test_id = $lr->test_id;
                }

                $result->save();
            }
        }
        $bar->finish();
        print_r('Done');
    }
}
