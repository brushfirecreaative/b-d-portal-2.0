<?php

namespace Bdiportal\Console\Commands;

use Illuminate\Console\Command;
use Bdiportal\Dealer;

class AssignIds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dealers:assignid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign Unique Ids to Dealers without them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dealers = Dealer::all();

        foreach($dealers as $dealer)
        {
            $unique_id = str_random(8);

            $dealer->unique_id = $unique_id;

            $dealer->save();
        }
    }
}
