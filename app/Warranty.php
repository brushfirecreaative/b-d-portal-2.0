<?php

namespace Bdiportal;

use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
	protected $table = 'warranty_cards';
    protected $fillable = ['serial_number','warranty_info'];

    public function notes(){
    	return $this->hasMany('Bdiportal\Note');
    }

}
