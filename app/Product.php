<?php namespace Bdiportal;

class Product extends \Eloquent {
	protected $fillable = [];

  public function accessories()
  {
    return $this->belongsToMany('Bdiportal\Accessory');
  }
}