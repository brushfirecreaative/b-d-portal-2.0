<?php namespace Bdiportal\Forms;

use Illuminate\Validation\Factory as Validator;
use Illuminate\Validation\Validator as ValidatorInstance;
use Bdiportal\Forms\FormValidationException;

abstract class FormValidator {


    /**
     * 
     * @var Validator
     *      
     */
    protected $validator;

    /**
     * 
     * @var ValidatorInstance
     *      
     */
    protected $validation;
    /**
     * 
     * @param  Validator $validator [description]
     * @return [type]               [description]
     */
    function __construct(Validator $validator)
    {
      $this->validator = $validator;
    }
    /**
     * [validate description]
     * @param  array  $formData [description]
     * @return [type]           [description]
     */
    public function validate(array $formData)
    {
      $this->validation = $this->validator->make($formData, $this->getValidationRules());

      if ($this->validation->fails())
      {
        throw new FormValidationException('Validation failed', $this->getValidationErrors());
      }

      return true;
    }
    /**
     * Get the validation rules
     * @return   array
     */

    protected function getValidationRules()
    {
      return $this->rules;
    }
    /**
     * Get the validation errors
     * @return  \Illuminate\Support\MessageBag
     */
    protected function getValidationErrors()
    {
      return $this->validation->errors();
    }
}