<?php namespace Bdiportal\Forms;

class OrderForm extends FormValidator {

  /**
   * Validation Rules for Order Form
   * @var [type]
   */
  protected $rules = [

      'shipping_address' => 'required|integer',
      'ordered_by' => 'required',
      'email' => 'required|email',
      'po' => '',
      'vehicle_year' =>'required',
      'vehicle_make' => 'required',
      'vehicle_model' => 'required',
      'drivers_seat' => 'required',
      'passenger_seat' => 'required',
      'ramp' => 'required',
      'interior_color' => 'required',
      'conversion_by' => 'required|exists:conversion_companies,id',
      'conversion_type' => 'required|exists:conversions,id',
      'conversion_year' => 'required',
      'seatbase[]' => 'required',
      



  ];
}