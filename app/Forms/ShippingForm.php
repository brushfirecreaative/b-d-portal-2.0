<?php namespace Bdiportal\Forms;

class ShippingForm extends FormValidator {

  /**
   * Validation Rules for User Information
   * @var [type]
   */
  protected $rules = [

      'street1' => 'required',
      'city' => 'required',
      'state' => 'required',
      'postal_code' => 'required',
      'country' => 'required'
  ];
}