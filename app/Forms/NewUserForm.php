<?php namespace Bdiportal\Forms;

class NewUserForm extends FormValidator {

  /**
   * Validation Rules for User Information
   * @var [type]
   */
  protected $rules = [

      'username' => 'required|unique:users',
      'prop_name' => 'required',
      'email' => 'required|email|unique:users',
      'phone' => 'alpha_dash',
      'fax' =>'alpha_dash',
      'password' => 'required|min:8|confirmed'

  ];
}