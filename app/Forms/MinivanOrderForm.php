<?php namespace Bdiportal\Forms;

class MinivanOrderForm extends FormValidator {

  /**
   * Validation Rules for Order Form
   * @var [type]
   */
  protected $rules = [

      'shipping_address' => 'required|integer',
      'ordered_by' => 'required',
      'email' => 'required|email',
      'po' => '',
      'vehicle_year' =>'required',
      'vehicle_make' => 'required',
      'vehicle_model' => 'required',
      'drivers_seat' => 'required',
      'passenger_seat' => 'required',
      'ramp' => 'required',
      'interior_color' => 'required',
      'conversion_by' => 'required|exists:conversion_companies,id',
      'conversion_type' => 'required|exists:conversions,id',
      'conversion_year' => 'required',
      '1-option1' => 'required_if:seatbase.0, 1',
      '1-option2' => 'required_if:seatbase.0, 1',
      '1-option3' => 'required_if:seatbase.0, 1',
      '1-option4' => 'required_if:seatbase.0, 1',
      '1-option5' => 'required_if:seatbase.0, 1',
      '1-option6' => 'required_if:seatbase.0, 1',
      '1-option7' => 'required_if:seatbase.0, 1',
      '2-option1' => 'required_if:seatbase.1, 2',
      '2-option2' => 'required_if:seatbase.1, 2',
      '2-option3' => 'required_if:seatbase.1, 2',
      '2-option4' => 'required_if:seatbase.1, 2',
      '2-option5' => 'required_if:seatbase.1, 2',
      '2-option6' => 'required_if:seatbase.1, 2',
      '2-option7' => 'required_if:seatbase.1, 2',



      



  ];
}