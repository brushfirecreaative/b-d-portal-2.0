<?php namespace Bdiportal\Forms;

class UserForm extends FormValidator {

  /**
   * Validation Rules for User Information
   * @var [type]
   */
  protected $rules = [

      'username' => 'required',
      'prop_name' => 'required',
      'email' => 'required|email',
      'phone' => 'alpha_dash',
      'fax' =>'alpha_dash',
      'password' => 'min:8|confirmed'

  ];
}