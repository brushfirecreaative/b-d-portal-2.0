<?php

namespace Bdiportal;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    

    public function role()
    {
    	return $this->belongsTo('Bdiportal\Role');
    }

    public static function getRoleByName($position)
    {
    	$query = Position::where('name', '=', $position)->first();

    	return $query->role;
    }

    public static function positionSelects()
    {
    	$ps = Position::all();
		$positions[] = 'Please chose this users postion...';
		foreach($ps as $p)
		{
			$positions[$p->name] = $p->name;
		}
		return $positions;
    }
}
