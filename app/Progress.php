<?php namespace Bdiportal;

class Progress extends \Eloquent {
	protected $fillable = ['certificate_id, user_id, next_text, progress'];
  protected $table = 'progress';


  public static function getExipiringTechs()
  {

  	$techs = Progress::where('progress', '=', '100')->get();
  	foreach($techs as $tech){

  		$today = new \DateTime();
  		$completed = new \DateTime($tech->completed_on);
  		$diff = $today->diff($completed);
  		if($diff->days == 716) //Exires in two weeks
  		{
  			$expiringTechs[] = array(
  				'user_id' => $tech->user_id, 
  				'certificate_id' => $tech->certificate_id
  			);
  		}
  	}
  	return $expiringTechs;
   }

   public static function expiringSoon($user, $cert)
   {
   		$tech = Progress::where('progress', '=', '100')
   		->where('user_id', '=', $user)
   		->where('certificate_id', '=', $cert)
   			->first();
   		if($tech)
   		{
	   		$today = new \DateTime();
	  		$completed = new \DateTime($tech->completed_on);
	  		$diff = $today->diff($completed);
	  		if($diff->days >= 716 && $diff->days <= 730) //Exires in two weeks
	  		{
	  			return true; //User 2 weeks away from expiration
	  		}
	  		else
	  		{
	  			return false; //User not 2 weeks away from expiration
	  		}
	  	}
	  	else
	  	{
	  		return false; // User hasn't finished
	  	}
   }

    public static function expired($user, $cert)
   {
      $tech = Progress::where('progress', '=', '100')
      ->where('user_id', '=', $user)
      ->where('certificate_id', '=', $cert)
        ->first();
      if($tech)
      {
        $today = new \DateTime();
        $completed = new \DateTime($tech->completed_on);
        $diff = $today->diff($completed);
        if($diff->days >= 730) // Years or over
        {
          // $tech->progress = 0;
          // $tech->save();
          return true; //It's been 2 years or more
        }
        else
        {
          return false; //It's has not been 2 years or more
        }
      }
      else
      {
        return false; // User hasn't finished
      }
   }



}