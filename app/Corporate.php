<?php namespace Bdiportal;

class Corporate extends \Eloquent {
	protected $fillable = [];

	public function dealers()
	{
		return $this->hasMany('Bdiportal\Dealer');
	}

	public function users()
	{
		return $this->hasManyThrough('Bdiportal\User', 'Bdiportal\Dealer');
	}
}