<?php namespace Bdiportal;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Bdiportal\PasswordHash;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, EntrustUserTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public static function checkConfirmed($username)
	{
		$query = User::where('username', '=', $username)->first();
		if($query) return $query->confirmed;	

		return false;
		
	}

	public static function isLegacy($username)
	{
		return \DB::table('legacy_users')->where('username', $username)->first();
	}

	public static function legacyUserId($username, $password)
	{
		$query = \DB::table('legacy_users')->where('username', $username)->first();
		return $query->id;
		
	}
	public static function exsists($username)
	{
		$query = User::where('username', '=', $username)->first();
		if($query) return true;	

		return false;
	}

	public function dealer()
	{
		return $this->belongsTo('Bdiportal\Dealer');
	}

	/**
	 * [roles description]
	 * @return [type] [description]
	 */
	public function roles()
	{
		return $this->belongsToMany('Bdiportal\Role');
	}
	/**
	 * Check for Role by name
	 * @param  [type]  $name [description]
	 * @return boolean       [description]
	 */
	public function hasRole($name)
	{
		foreach ($this->roles as $roles)
		{
			if ($roles->name == $name) return true;
		}
		return false;
	}

	/**
	 * Check for Role id or Higher
	 * @param  [type]  $name [description]
	 * @return boolean       [description]
	 */
	public function hasRoleOrHigher($id)
	{
		foreach ($this->roles as $roles)
		{
			if ($roles->id >= $id) return true;
		}
		return false;
	}

	/**
	 * Assign a Role to User
	 * @param  [type] $role [description]
	 * @return [type]       [description]
	 */
	public function assignRole($role)
	{
		return $this->roles()->attach($role);
	}

	/**
	 * Revoke a Role to User
	 * @param  [type] $role [description]
	 * @return [type]       [description]
	 */
	public function revokeRole($role)
	{
		return $this->roles()->detach($role);
	}

	public static function search($term, $dealer_id)
	{
		return User::where('dealer_id', '=', $dealer_id)
					->where(function($query) use ($term){
						$query->where('email', 'LIKE', "%$term%")
						->orWhere('prop_name', 'LIKE', "%$term%");
					});
					
	}
}
