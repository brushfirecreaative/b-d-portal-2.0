//Init Validator
$(document).ready( function(){
  $('.orderForm').validate();
});

$('input:radio[name=options\\[1\\]\\[3\\]\\[\\]]').change(function() {
  console.log(this.value);
  if(this.value == 24) {
    $('input:radio[name=options\\[1\\]\\[4\\]\\[\\]]').parents('.well').hide();
  } 
  else{

  }
});
// Using the Selected Year to Populate the Make/Model Field
$('#v_year').change(function() { 
  var v_year = $('#v_year').val();
  var _token = $("input[name=_token]").val();
  console.log(_token);
  $.ajax({
    type: "POST",
    url:  "/order/makeModel", 
    data: { v_year: v_year, _token: _token }
  })
    .done(function( data ) {
      //For debuging
      console.log(data);

      var $makeModel = $("#make_model");
      $makeModel.empty();
       $makeModel.append($("<option></option>")
          .attr("value", '').text('Choose Make/Model...'));
      $.each(data, function(value, key) {
        $makeModel.append($("<option></option>")
          .attr("value", value).text(key));
      })

      
    });
});
// Using the Make/Model Field to populate the Conversion Company
$('#make_model').change(function() { 
  var mm = $('#make_model').val();
  var v_year = $('#v_year').val();
  var _token = $("input[name=_token]").val();
  var form_id = $('#form_id').val();
  $.ajax({
    type: "POST",
    url:  "/order/conversionBy", 
    data: { mm: mm,  v_year: v_year, _token: _token, form_id: form_id}
  })
    .done(function( data ) {
      //For debuging
      console.log(data);

      var $cb = $("#cb");
      $cb.empty();
       $cb.append($("<option></option>")
          .attr("value", '').text('Conversion By...'));
      $.each(data, function(value, key) {
        $cb.append($("<option></option>")
          .attr("value", value).text(key));
      })

      
    });
});
//Populating Conversion Type Select
  $('#cb').change(function() {
    var cb = $('#cb').val();
    var _token = $("input[name=_token]").val();
    $.ajax({
      type: "POST",
      url:  "/order/conversions", 
      data: { cb: cb, _token: _token}
    })
      .done(function( data ) {

        var options = $("#ct");
        options.find('option').remove();
        options.append($("<option></option>")
          .attr("value", '').text('Conversion Type...'));
        $.each(data, function() {
          options.append($("<option />").val(this.id).text(this.name));
        });
      });
});
//Populating Conversion Type Select
  $('#ct').change(function() {
    var ct = $('#ct').val();
    var _token = $("input[name=_token]").val();
    $.ajax({
      type: "POST",
      url:  "/order/minivans", 
      data: { ct: ct, _token: _token}
    })
      .done(function( data ) {

        $(".seatbase").html(data);
          
      });
});

  //Populating Conversion Type Select
  $('#make_model_special').change(function() {
    var ct = $('#ct').val();
    var _token = $("input[name=_token]").val();
    $.ajax({
      type: "POST",
      url:  "/order/minivans", 
      data: { ct: ct, _token: _token}
    })
      .done(function( data ) {

        $(".seatbase").html(data);
          
      });
});



  //**HighTower Functions**********************

  //Populating Conversion Type Select
  $('#ht_make').change(function() {
    var make = $('#ht_make').val();
    var _token = $("input[name=_token]").val();
    $.ajax({
      type: "POST",
      url:  "/order/wc_models", 
      data: { make: make, _token: _token}
    })
      .done(function( data ) {
        console.log(data);

        var options = $("#ht_model");
        options.find('option').remove();
        options.append($("<option></option>")
          .attr("value", '').text('Wheelchair Model...'));
        $.each(data.models, function(value, key) {
          options.append($("<option />").val(value).text(key));
        });

        var a_options = $("#ht_ap");
        a_options.find('option').remove();
        a_options.append($("<option></option>")
          .attr("value", '').text('Application...'));
        $.each(data.ap, function(value, key) {
          a_options.append($("<option />").val(value).text(key));
        });
      });
});


//Populating Conversion Type Select
  $('#ht_ap').change(function() {
    var ap = $('#ht_ap').val();
    if(ap == '4'){
      console.log('3 || 4');
      $('#ht_action_div').addClass('hidden');
      $('#ht_action').prop('selectedIndex',0);
      $('#ht_located_div').removeClass('hidden');
      $('#ht_vehicle_div').addClass('hidden');
      $('#ht_vehicle').prop('selectedIndex',0);
      $('#ht_conversion_div').addClass('hidden');
      $('#ht_conversion').prop('selectedIndex',0);
      $('#ht_conversion_other').addClass('hidden');
    } 
    if(ap == '3'){
      console.log('3');
      $('#ht_located_div').removeClass('hidden');
      $('#ht_action_div').removeClass('hidden');
      $('#ht_vehicle_div').addClass('hidden');
      $('#ht_vehicle').prop('selectedIndex',0);
      $('#ht_conversion_div').addClass('hidden');
      $('#ht_conversion').prop('selectedIndex',0);
      $('#ht_conversion_other').addClass('hidden');
    }
    if(ap == '1' || ap =='2')
    {
      $('#ht_located_div').addClass('hidden');
      $('#ht_located').prop('selectedIndex',0);
      $('#ht_action_div').addClass('hidden');
      $('#ht_action').prop('selectedIndex',0);
      $('#ht_vehicle_div').removeClass('hidden');
      $('#ht_conversion_div').removeClass('hidden');
     

    }
  });

  // $('#ht_vehicle').change(function() {
  //   var ht_vehicle = $('#ht_vehicle').val();
  //   if(ht_vehicle == '13')
  //   {
  //      $('#ht_conversion_div').removeClass('hidden');
  //   }
  //   else{
  //      $('#ht_conversion_div').addClass('hidden');
  //      $('#ht_conversion').prop('selectedIndex',0);
  //   }
  // });

  $('#ht_conversion').change(function() {
    var conv = $('#ht_conversion').val();

    if(conv == 'Other')
    {
      $('#ht_conversion_other_div').removeClass('hidden');
    }
    else{
      $('#ht_conversion_other_div').addClass('hidden');
    }
  });

  $('#get_config').click(function(e) {
    e.preventDefault();

      var ap =  $('#ht_ap').val(),
          make = $('#ht_make').val(),
          model = $('#ht_model').val(),
          vehicle = $('#ht_vehicle').val();
          side = $('#ht_located').val();
          conversion = $('#ht_conversion').val();
          ht_action = $('#ht_action').val();

       console.log(ap + ' ' +ht_action+ ' ' +side);
     var _token = $("input[name=_token]").val();
    $.ajax({
      type: "POST",
      url:  "/order/get_ht", 
      data: { ap: ap, make: make, model: model, vehicle: vehicle, side: side, conversion: conversion, ht_action: ht_action, _token: _token}
    })
      .done(function( data ) {
        console.log(data);
        $('#hightower').removeClass('hidden');
        $('#graphic').removeClass('hidden');
        $('#ht_bracket').html(data.bracket);
        $('#ht_station').html(data.station);
        $('#ht_floor_mount').html(data.floor_mount);
        if(data.graphic){
          $('#ht_graphic').html('<img src="'+data.graphic+'" width="100%" />');
        }
        else{
            $('#ht_graphic').html(' ');
        }
      })
      .fail(function() {
        alert('We were unable to find your Bracket and Station. Please make sure you have the Wheelchair Make, Model and Aplication fields filled out.');
      });
      

  });

